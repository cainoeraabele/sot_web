/* eslint-disable @next/next/no-sync-scripts */
import React, { useEffect, useState } from 'react';
import { useTheme , themes } from '../lib/ThemeContext'
import Ball from '../componets/ballYo'
import { useRouter } from 'next/router'
import Head from 'next/head'
import Script from 'next/script'



const Layout =({mobile,children,allText}) =>{
    const router = useRouter()
    const [lang,setLang] = useState('IT')
    const { theme, setTheme } = useTheme()


    const goHome = () =>{
        let urlH = router.pathname
        let arrr = urlH.split('/')

        if(arrr[1] == 'it'){
            router.push('/it')
        }else{
            router.push('/')
        }
    }

    const toggleTheme = () => {
        setTheme(theme === themes.dark ? themes.light : themes.dark)
      }

     useEffect(()=>{
        console.log('vediamo r');
        let urlH = router.pathname
        console.log(urlH);
        let arrr = urlH.split('/')
        console.log(arrr);
        if(arrr[1] == 'it'){
            setLang('EN')
        }else{
            setLang('IT')
        }
        
     }) 

    const cambioLingua = () =>{
        let urlH = router.pathname
        let arrr = urlH.split('/')
        if(arrr[1] == 'it'){
            if(arrr.length > 2){
                //console.log('/'+arrr[2]);
                router.push('/'+arrr[2])  
            }else{
                router.push('/')  
                //console.log('/');
            }
            
        }else{
            //console.log('/it/'+arrr[1]);
            router.push('/it/'+arrr[1])
        }
        //router.push(href)
    }  
    if(mobile){
        return(
            <div style={{backgroundColor: theme?.background}}>
                <Head>
                    <title>Soundsofthings</title>
                    <script dangerouslySetInnerHTML={{ __html: `var _iub = _iub || []; _iub.csConfiguration = {"askConsentAtCookiePolicyUpdate":true,"countryDetection":true,"enableLgpd":true,"enableUspr":true,"lgpdAppliesGlobally":false,"perPurposeConsent":true,"siteId":811869,"whitelabel":false,"cookiePolicyId":8120422,"lang":"en", "banner":{ "acceptButtonDisplay":true,"closeButtonDisplay":false,"customizeButtonDisplay":true,"explicitWithdrawal":true,"listPurposes":true,"logo":null,"position":"float-top-center","rejectButtonDisplay":true }}` }} />
                    <script type="text/javascript" src="//cdn.iubenda.com/cs/gpp/stub.js"></script>
                    <script type="text/javascript" src="//cdn.iubenda.com/cs/iubenda_cs.js" charset="UTF-8" async></script>
                </Head>
                <div style={{display:'flex',justifyContent: 'space-between',position:'relative',zIndex:3,alignItems: 'center',paddingRight:15,  position: 'sticky',top: 0, background: theme.background}}>
                    <img onClick={()=>goHome()} style={{height:73,padding:10,}} src={theme.logo} />
                    {theme === themes.dark ? (
                        <div  className='flex'>
                        <div style={{height:21,width:41, borderRadius:10, border:theme.border,display:'flex',alignItems: 'center',justifyContent: 'space-between'}}>
                            <div  onClick={toggleTheme} style={{marginLeft:3,cursor:'pointer'}}>
                                <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M9.69436 5.9928C9.67311 4.01601 8.13195 2.30521 5.98957 2.30896C3.76532 2.3127 2.30228 4.17086 2.30665 6.01278C2.31228 7.84346 3.78219 9.68475 5.99957 9.68475C8.15195 9.68475 9.68186 7.95522 9.69436 5.9928ZM8.89253 6.00091C8.9319 7.53252 7.6076 8.90053 5.99269 8.89679C4.55527 8.89679 3.09348 7.75355 3.09661 5.98968C3.09973 4.24141 4.54903 3.09193 6.00394 3.0938C7.62448 3.09755 8.9369 4.47243 8.89253 6.00091Z" fill="#E2E2E2"/>
                                    <path d="M5.60352 10.8536C5.60352 11.0715 5.60352 11.2906 5.60352 11.5067C5.60319 11.584 5.615 11.6609 5.63852 11.7346C5.66581 11.8117 5.71671 11.8783 5.78402 11.925C5.85132 11.9716 5.93161 11.9958 6.0135 11.9943C6.09668 11.9903 6.17647 11.9601 6.24146 11.9081C6.30645 11.8561 6.35331 11.7848 6.37535 11.7046C6.39551 11.6297 6.4058 11.5524 6.40598 11.4748C6.40598 11.0702 6.40598 10.6631 6.40598 10.2617C6.4058 10.1917 6.39805 10.122 6.38285 10.0537C6.33473 9.8452 6.18224 9.72532 5.98538 9.73406C5.88593 9.73406 5.79055 9.77353 5.72023 9.84379C5.6499 9.91405 5.6104 10.0093 5.6104 10.1087C5.59852 10.3584 5.6104 10.6082 5.6104 10.8548L5.60352 10.8536Z" fill="#E2E2E2"/>
                                    <path d="M5.60416 1.14761C5.60416 1.38113 5.60416 1.61465 5.60416 1.84754C5.60098 1.91806 5.61668 1.98816 5.64963 2.0506C5.68259 2.11305 5.73161 2.16559 5.79165 2.20281C5.84089 2.23458 5.89675 2.25467 5.95496 2.26156C6.01317 2.26844 6.07218 2.26194 6.12749 2.24253C6.18279 2.22313 6.23292 2.19135 6.27404 2.14962C6.31516 2.10789 6.34619 2.05731 6.36474 2.00176C6.39141 1.91244 6.40447 1.81962 6.40349 1.72641C6.40724 1.32993 6.40349 0.933448 6.40349 0.536343C6.40404 0.466642 6.39671 0.397102 6.38161 0.329049C6.36537 0.236503 6.31687 0.152687 6.24468 0.0924578C6.1725 0.0322285 6.08131 -0.000524998 5.98726 1.19209e-06C5.88873 0.0056746 5.79573 0.0473776 5.72599 0.11716C5.65626 0.186943 5.61467 0.279923 5.60916 0.378375C5.59541 0.587542 5.60478 0.797958 5.60353 1.00775V1.14761H5.60416Z" fill="#E2E2E2"/>
                                    <path d="M3.55059 8.99476C3.54999 8.9253 3.53114 8.85722 3.49591 8.79733C3.46069 8.73744 3.41034 8.68784 3.3499 8.65351C3.28946 8.61918 3.22105 8.60131 3.15153 8.6017C3.082 8.60209 3.0138 8.62072 2.95374 8.65572C2.89466 8.69337 2.84103 8.73897 2.79438 8.79121C2.49752 9.1034 2.20316 9.41559 1.9063 9.72216C1.85626 9.7659 1.81635 9.82 1.78937 9.88071C1.76238 9.94142 1.74897 10.0073 1.75006 10.0737C1.75055 10.1382 1.76772 10.2016 1.79992 10.2575C1.83212 10.3135 1.87824 10.3602 1.93382 10.3931C1.98941 10.426 2.05256 10.4441 2.11716 10.4454C2.18176 10.4468 2.24561 10.4314 2.30253 10.4009C2.37696 10.3589 2.44425 10.3053 2.50189 10.2423C2.79438 9.93945 3.08311 9.63225 3.37685 9.32756C3.47172 9.24018 3.53316 9.1225 3.55059 8.99476Z" fill="#E2E2E2"/>
                                    <path d="M3.54843 2.96268C3.52544 2.88799 3.4904 2.81755 3.44469 2.75414C3.10012 2.37951 2.75035 2.01217 2.39537 1.65211C2.35217 1.60603 2.29728 1.57252 2.23653 1.55515C2.17578 1.53778 2.11145 1.53719 2.05039 1.55346C1.991 1.56761 1.93569 1.59528 1.88878 1.63431C1.84186 1.67334 1.8046 1.72268 1.77992 1.77847C1.75524 1.83425 1.74379 1.89499 1.74648 1.95592C1.74916 2.01686 1.76591 2.07635 1.7954 2.12976C1.82279 2.17529 1.85575 2.21724 1.89352 2.25463C2.19913 2.57806 2.50724 2.89837 2.8116 3.22242C2.92659 3.3473 3.06158 3.40974 3.22845 3.38289C3.32356 3.36321 3.40834 3.30983 3.46714 3.2326C3.52595 3.15538 3.55482 3.05951 3.54843 2.96268Z" fill="#E2E2E2"/>
                                    <path d="M10.2617 10.0094C10.2327 9.92979 10.192 9.85489 10.1411 9.78711C9.80738 9.42622 9.46928 9.06907 9.12367 8.71942C9.08794 8.68028 9.04439 8.64907 8.99582 8.62782C8.94726 8.60656 8.89476 8.59573 8.84174 8.59602C8.78872 8.59632 8.73635 8.60773 8.68803 8.62953C8.6397 8.65132 8.5965 8.68301 8.5612 8.72254C8.40871 8.88426 8.41621 9.09717 8.59245 9.28448C8.86869 9.58169 9.14992 9.8739 9.43053 10.1674C9.49752 10.2435 9.57137 10.3133 9.65114 10.3759C9.70652 10.4204 9.77447 10.4464 9.84543 10.4503C9.91639 10.4543 9.98679 10.4359 10.0467 10.3978C10.1767 10.3247 10.2499 10.2129 10.2617 10.0094Z" fill="#E2E2E2"/>
                                    <path d="M10.2645 1.98238C10.2464 1.77071 10.1683 1.65458 10.0214 1.58402C9.95547 1.54811 9.87956 1.53477 9.80529 1.54605C9.73103 1.55732 9.6625 1.59259 9.6102 1.64646C9.45084 1.79506 9.3021 1.95553 9.15086 2.1135C8.97274 2.30081 8.79712 2.48813 8.62151 2.67544C8.57785 2.71978 8.53947 2.76901 8.50714 2.82217C8.46103 2.90022 8.4448 2.99232 8.46146 3.08141C8.47812 3.17051 8.52655 3.25054 8.59776 3.30669C8.6653 3.36033 8.74847 3.39056 8.83472 3.39281C8.92097 3.39506 9.00561 3.36922 9.07586 3.31918C9.11268 3.2903 9.14697 3.25834 9.17835 3.22365C9.49084 2.89647 9.80644 2.57179 10.1121 2.23837C10.1726 2.15923 10.2238 2.07332 10.2645 1.98238Z" fill="#E2E2E2"/>
                                    <path d="M10.8745 6.38357C11.1088 6.38357 11.3432 6.38732 11.5776 6.38357C11.8313 6.37858 11.9919 6.23123 11.9975 6.00895C12.0032 5.7748 11.8388 5.61559 11.5744 5.61309C11.0982 5.60872 10.622 5.61309 10.1451 5.61309C10.0706 5.60853 9.99634 5.62592 9.93158 5.6631C9.86682 5.70028 9.81439 5.75561 9.78079 5.82226C9.74518 5.88364 9.7286 5.9542 9.73316 6.02501C9.73772 6.09581 9.76322 6.16367 9.80642 6.21999C9.84407 6.27497 9.89537 6.31925 9.95529 6.34847C10.0152 6.3777 10.0817 6.39087 10.1483 6.3867C10.3883 6.38233 10.6301 6.38357 10.8745 6.38357Z" fill="#E2E2E2"/>
                                    <path d="M1.13994 5.61064C0.898075 5.61064 0.656213 5.61064 0.414977 5.61064C0.17374 5.61064 0 5.78172 0 5.99525C0.000697981 6.04791 0.0119899 6.09989 0.0332041 6.14811C0.0544183 6.19632 0.0851214 6.23977 0.123487 6.27589C0.161852 6.31201 0.207097 6.34004 0.256529 6.35834C0.30596 6.37663 0.35857 6.3848 0.411227 6.38237C0.894533 6.38695 1.37784 6.38695 1.86115 6.38237C1.96325 6.38287 2.06137 6.34282 2.13392 6.27104C2.20647 6.19926 2.2475 6.10163 2.248 5.99962C2.2485 5.89762 2.20841 5.79959 2.13657 5.72711C2.06472 5.65463 1.967 5.61363 1.8649 5.61313C1.62491 5.60627 1.3818 5.61064 1.13994 5.61064Z" fill="#E2E2E2"/>
                                </svg>
                            </div>
                            <div onClick={toggleTheme} style={{width:15,height:15,borderRadius:20,marginRight:3,marginTop:1}}>
                                <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                 <circle cx="7.5" cy="7.5" r="7.5" fill="#E2E2E2"/>
                                </svg>
                            </div>
                        </div>
                        <div style={{marginLeft:11}} className='ml-2'>
                             <p onClick={()=>cambioLingua()} style={{cursor:'pointer', fontSize:13, color:theme.colorT}}>{lang}</p>
                        </div>    
                        
                        </div>
                    ) : (
                        <div  className='flex'>
                        <div style={{height:21,width:41, borderRadius:10, border:theme.border , display:'flex',alignItems: 'center',justifyContent: 'space-between'}}>
                            <div  style={{borderRadius:20,marginLeft:3,marginTop:1}}>
                                <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="7.5" cy="7.5" r="7.5" fill="#3E3E3E"/>
                                </svg>
                            </div>
                            <div  onClick={toggleTheme} style={{marginRight:3,cursor:'pointer'}}>
                              <svg width="10" height="11" viewBox="0 0 10 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M4.50321 0.698883C2.26523 1.45901 0.708375 3.67735 0.950757 6.06241C1.01904 6.727 1.22579 7.3672 1.55888 7.94542C1.89197 8.52364 2.34469 9.02824 2.89046 9.4296C3.43623 9.83096 4.06406 10.121 4.7371 10.2827C5.41014 10.4444 6.11482 10.4744 6.80979 10.3711C7.61639 10.2594 8.38959 9.96333 9.06343 9.50808C9.10576 9.4823 9.14594 9.45334 9.18357 9.42147C9.23163 9.38155 9.26814 9.33043 9.28931 9.27338C9.31049 9.21634 9.31556 9.15544 9.30402 9.09698C9.29248 9.03852 9.26473 8.98462 9.22365 8.94085C9.18257 8.89707 9.12964 8.86501 9.07033 8.84798C9.00306 8.82916 8.93461 8.81397 8.86531 8.80249C7.36036 8.45432 6.21702 7.64942 5.49302 6.34559C4.77515 5.05735 4.68515 3.69538 5.18825 2.27945C5.34053 1.85653 5.55296 1.45399 5.8194 1.08345C5.85183 1.03818 5.88641 0.993868 5.91489 0.945986C5.98761 0.826313 6.01724 0.701255 5.93687 0.580166C5.8994 0.518647 5.84237 0.470567 5.77376 0.442654C5.70515 0.414742 5.62838 0.408394 5.55422 0.4245C5.31647 0.467924 5.08035 0.521084 4.84621 0.579845C4.73001 0.613881 4.61552 0.653614 4.50321 0.698883ZM4.83598 1.29726C4.05166 2.86996 3.92566 4.4464 4.5513 6.03365C5.17694 7.6209 6.36008 8.67696 8.00409 9.28987C7.04134 9.82061 5.22984 10.0352 3.75328 9.14248C2.15406 8.17517 1.43905 6.74269 1.70491 4.91457C1.96472 3.12753 3.0907 1.95243 4.8349 1.29765L4.83598 1.29726Z" fill="#333333"/>
                              </svg>
                            </div>
                        </div>
                        <div style={{marginLeft:11}} className='ml-2'>
                             <p onClick={()=>cambioLingua()} style={{cursor:'pointer', fontSize:13, color:theme.colorT}}>{lang}</p>
                        </div> 
                        </div>
                    )}
                    
                    
                </div>
                <main className={''}>
                <main style={{display:'none'}}>{children}</main>
                
               
                </main>
                
                <div style={{padding:20, display:'flex',gap:mobile? 6: 26,height:mobile? 'auto': 54}}>
                    <div>
                            <a target='_blank' href='https://www.instagram.com/soundsofthings/' rel="noreferrer"><img style={{height:30,width:30}} src={theme.in}/></a>
                        </div>
                        <div>
                            <a  target='_blank' href='https://www.facebook.com/globalsoundvillage/' rel="noreferrer"><img style={{height:30,width:30}} src={theme.fb} /></a>
                        </div>
                        <div>
                            <a  target='_blank' href='https://twitter.com/SoundsOfThings' rel="noreferrer"><img style={{height:30,width:30}} src={theme.tw} /></a>
                        </div>
                        {/*<div>
                            <a  target='_blank' href=''><img style={{height:30,width:30}} src={theme.vm} /></a>
                        </div>*/}
                        <div>
                            <a  target='_blank' href='https://www.youtube.com/channel/UCyp56Ox_awg9N5xt8fn5axw' rel="noreferrer"><img style={{height:30,width:30}} src={theme.yt} /></a>
                        </div>
                        <div>
                            <a  target='_blank' href='https://t.me/soundsofthings' rel="noreferrer"><img style={{height:30,width:30}} src={theme.tk} /></a>
                        </div>
                     </div>
                     <div>
                    </div>
                <footer style={{background:' #e6e6e5',color:theme.colorT}} className={mobile ? 'fottersMobile' : 'fotters'}>
                    <p>SOUND OF THINGS</p>
                    <div className={mobile ? 'fottersMobileItems' : 'fottersItems'}>
                    <div>
                        <p>Head Quarter</p>
                        <p>Via scuderie 7,</p> 
                        <p>39012 Merano (BZ)</p>
                    </div>
                    <div>
                        <p>{lang != 'IT' ? 'Unità operativa' :  'Operating Unit'}</p>
                        <p>Viale Principe Umberto 67/bis,</p> 
                        <p>98122 Messina (ME)</p>
                    </div>
                    <div style={{fontSize:8}}>
                        {lang != 'IT' ? (
                        <a target={'_blank'} href='/privacy' rel="noreferrer"><strong>Privacy</strong></a>
                        ):(
                         <a target={'_blank'} href='/it/privacy' rel="noreferrer"><strong>Privacy</strong></a>
  
                        )}
                        <br></br>
                        {lang != 'IT' ? (
                        <a target={'_blank'} href='/t&c' rel="noreferrer"><strong>Terms and Conditions</strong></a>
                        ):(
                         <a target={'_blank'} href='/it/t&c' rel="noreferrer"><strong>Termini e Condizioni</strong></a>
  
                        )}
                        <br></br>
                        {lang != 'IT' ? (
                            // eslint-disable-next-line @next/next/no-html-link-for-pages
                            <a target={'_blank'} href='/it/r&d'><strong>R&D riconoscimenti e contributi</strong></a>
                        ) : (
                            // eslint-disable-next-line @next/next/no-html-link-for-pages
                            <a target={'_blank'} href='/r&d'><strong>R&D awards and grants</strong></a>
                        )}
                        <br></br>
                        <a href='#' class='iubenda-cs-preferences-link'>{lang != 'IT' ? 'Preferenze privacy' : 'Privacy preferences'}</a>

                    </div>
                    </div>
                    <br></br>
                </footer>
                
            </div>
        )
    }else{
        return(
            <div style={{backgroundColor: theme?.background,height:'100vh',width:'100vw'}}>
                <Head>
                    <title>Soundsofthings</title>
                    { lang != 'EN' ? (
                        <script dangerouslySetInnerHTML={{ __html: `var _iub = _iub || []; _iub.csConfiguration = {"askConsentAtCookiePolicyUpdate":true,"countryDetection":true,"enableLgpd":true,"enableUspr":true,"lgpdAppliesGlobally":false,"perPurposeConsent":true,"siteId":811869,"whitelabel":false,"cookiePolicyId":8120422,"lang":"en", "banner":{ "acceptButtonDisplay":true,"closeButtonDisplay":false,"customizeButtonDisplay":true,"explicitWithdrawal":true,"listPurposes":true,"logo":null,"position":"float-top-center","rejectButtonDisplay":true }}` }} />

                    ) : (
                        <script dangerouslySetInnerHTML={{ __html: `var _iub = _iub || []; _iub.csConfiguration = {"askConsentAtCookiePolicyUpdate":true,"countryDetection":true,"enableLgpd":true,"enableUspr":true,"lgpdAppliesGlobally":false,"perPurposeConsent":true,"siteId":811869,"whitelabel":false,"cookiePolicyId":8120422,"lang":"en", "banner":{ "acceptButtonDisplay":true,"closeButtonDisplay":false,"customizeButtonDisplay":true,"explicitWithdrawal":true,"listPurposes":true,"logo":null,"position":"float-top-center","rejectButtonDisplay":true }}` }} />

                    )}
                    <script type="text/javascript" src="//cdn.iubenda.com/cs/gpp/stub.js"></script>
                    <script type="text/javascript" src="//cdn.iubenda.com/cs/iubenda_cs.js" charset="UTF-8" async></script>
                </Head>
                <div style={{display:'flex',justifyContent: 'space-between',position:'relative',zIndex:3,alignItems: 'center',paddingRight:15}}>
                    <img onClick={()=>goHome()} style={{height:73,padding:10,cursor:'pointer'}} src={theme.logo} />
                    {theme === themes.dark ? (
                        <div  className='flex'>
                        <div style={{height:21,width:41, borderRadius:10, border:theme.border,display:'flex',alignItems: 'center',justifyContent: 'space-between'}}>
                            <div  onClick={toggleTheme} style={{marginLeft:3,cursor:'pointer'}}>
                                <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M9.69436 5.9928C9.67311 4.01601 8.13195 2.30521 5.98957 2.30896C3.76532 2.3127 2.30228 4.17086 2.30665 6.01278C2.31228 7.84346 3.78219 9.68475 5.99957 9.68475C8.15195 9.68475 9.68186 7.95522 9.69436 5.9928ZM8.89253 6.00091C8.9319 7.53252 7.6076 8.90053 5.99269 8.89679C4.55527 8.89679 3.09348 7.75355 3.09661 5.98968C3.09973 4.24141 4.54903 3.09193 6.00394 3.0938C7.62448 3.09755 8.9369 4.47243 8.89253 6.00091Z" fill="#E2E2E2"/>
                                    <path d="M5.60352 10.8536C5.60352 11.0715 5.60352 11.2906 5.60352 11.5067C5.60319 11.584 5.615 11.6609 5.63852 11.7346C5.66581 11.8117 5.71671 11.8783 5.78402 11.925C5.85132 11.9716 5.93161 11.9958 6.0135 11.9943C6.09668 11.9903 6.17647 11.9601 6.24146 11.9081C6.30645 11.8561 6.35331 11.7848 6.37535 11.7046C6.39551 11.6297 6.4058 11.5524 6.40598 11.4748C6.40598 11.0702 6.40598 10.6631 6.40598 10.2617C6.4058 10.1917 6.39805 10.122 6.38285 10.0537C6.33473 9.8452 6.18224 9.72532 5.98538 9.73406C5.88593 9.73406 5.79055 9.77353 5.72023 9.84379C5.6499 9.91405 5.6104 10.0093 5.6104 10.1087C5.59852 10.3584 5.6104 10.6082 5.6104 10.8548L5.60352 10.8536Z" fill="#E2E2E2"/>
                                    <path d="M5.60416 1.14761C5.60416 1.38113 5.60416 1.61465 5.60416 1.84754C5.60098 1.91806 5.61668 1.98816 5.64963 2.0506C5.68259 2.11305 5.73161 2.16559 5.79165 2.20281C5.84089 2.23458 5.89675 2.25467 5.95496 2.26156C6.01317 2.26844 6.07218 2.26194 6.12749 2.24253C6.18279 2.22313 6.23292 2.19135 6.27404 2.14962C6.31516 2.10789 6.34619 2.05731 6.36474 2.00176C6.39141 1.91244 6.40447 1.81962 6.40349 1.72641C6.40724 1.32993 6.40349 0.933448 6.40349 0.536343C6.40404 0.466642 6.39671 0.397102 6.38161 0.329049C6.36537 0.236503 6.31687 0.152687 6.24468 0.0924578C6.1725 0.0322285 6.08131 -0.000524998 5.98726 1.19209e-06C5.88873 0.0056746 5.79573 0.0473776 5.72599 0.11716C5.65626 0.186943 5.61467 0.279923 5.60916 0.378375C5.59541 0.587542 5.60478 0.797958 5.60353 1.00775V1.14761H5.60416Z" fill="#E2E2E2"/>
                                    <path d="M3.55059 8.99476C3.54999 8.9253 3.53114 8.85722 3.49591 8.79733C3.46069 8.73744 3.41034 8.68784 3.3499 8.65351C3.28946 8.61918 3.22105 8.60131 3.15153 8.6017C3.082 8.60209 3.0138 8.62072 2.95374 8.65572C2.89466 8.69337 2.84103 8.73897 2.79438 8.79121C2.49752 9.1034 2.20316 9.41559 1.9063 9.72216C1.85626 9.7659 1.81635 9.82 1.78937 9.88071C1.76238 9.94142 1.74897 10.0073 1.75006 10.0737C1.75055 10.1382 1.76772 10.2016 1.79992 10.2575C1.83212 10.3135 1.87824 10.3602 1.93382 10.3931C1.98941 10.426 2.05256 10.4441 2.11716 10.4454C2.18176 10.4468 2.24561 10.4314 2.30253 10.4009C2.37696 10.3589 2.44425 10.3053 2.50189 10.2423C2.79438 9.93945 3.08311 9.63225 3.37685 9.32756C3.47172 9.24018 3.53316 9.1225 3.55059 8.99476Z" fill="#E2E2E2"/>
                                    <path d="M3.54843 2.96268C3.52544 2.88799 3.4904 2.81755 3.44469 2.75414C3.10012 2.37951 2.75035 2.01217 2.39537 1.65211C2.35217 1.60603 2.29728 1.57252 2.23653 1.55515C2.17578 1.53778 2.11145 1.53719 2.05039 1.55346C1.991 1.56761 1.93569 1.59528 1.88878 1.63431C1.84186 1.67334 1.8046 1.72268 1.77992 1.77847C1.75524 1.83425 1.74379 1.89499 1.74648 1.95592C1.74916 2.01686 1.76591 2.07635 1.7954 2.12976C1.82279 2.17529 1.85575 2.21724 1.89352 2.25463C2.19913 2.57806 2.50724 2.89837 2.8116 3.22242C2.92659 3.3473 3.06158 3.40974 3.22845 3.38289C3.32356 3.36321 3.40834 3.30983 3.46714 3.2326C3.52595 3.15538 3.55482 3.05951 3.54843 2.96268Z" fill="#E2E2E2"/>
                                    <path d="M10.2617 10.0094C10.2327 9.92979 10.192 9.85489 10.1411 9.78711C9.80738 9.42622 9.46928 9.06907 9.12367 8.71942C9.08794 8.68028 9.04439 8.64907 8.99582 8.62782C8.94726 8.60656 8.89476 8.59573 8.84174 8.59602C8.78872 8.59632 8.73635 8.60773 8.68803 8.62953C8.6397 8.65132 8.5965 8.68301 8.5612 8.72254C8.40871 8.88426 8.41621 9.09717 8.59245 9.28448C8.86869 9.58169 9.14992 9.8739 9.43053 10.1674C9.49752 10.2435 9.57137 10.3133 9.65114 10.3759C9.70652 10.4204 9.77447 10.4464 9.84543 10.4503C9.91639 10.4543 9.98679 10.4359 10.0467 10.3978C10.1767 10.3247 10.2499 10.2129 10.2617 10.0094Z" fill="#E2E2E2"/>
                                    <path d="M10.2645 1.98238C10.2464 1.77071 10.1683 1.65458 10.0214 1.58402C9.95547 1.54811 9.87956 1.53477 9.80529 1.54605C9.73103 1.55732 9.6625 1.59259 9.6102 1.64646C9.45084 1.79506 9.3021 1.95553 9.15086 2.1135C8.97274 2.30081 8.79712 2.48813 8.62151 2.67544C8.57785 2.71978 8.53947 2.76901 8.50714 2.82217C8.46103 2.90022 8.4448 2.99232 8.46146 3.08141C8.47812 3.17051 8.52655 3.25054 8.59776 3.30669C8.6653 3.36033 8.74847 3.39056 8.83472 3.39281C8.92097 3.39506 9.00561 3.36922 9.07586 3.31918C9.11268 3.2903 9.14697 3.25834 9.17835 3.22365C9.49084 2.89647 9.80644 2.57179 10.1121 2.23837C10.1726 2.15923 10.2238 2.07332 10.2645 1.98238Z" fill="#E2E2E2"/>
                                    <path d="M10.8745 6.38357C11.1088 6.38357 11.3432 6.38732 11.5776 6.38357C11.8313 6.37858 11.9919 6.23123 11.9975 6.00895C12.0032 5.7748 11.8388 5.61559 11.5744 5.61309C11.0982 5.60872 10.622 5.61309 10.1451 5.61309C10.0706 5.60853 9.99634 5.62592 9.93158 5.6631C9.86682 5.70028 9.81439 5.75561 9.78079 5.82226C9.74518 5.88364 9.7286 5.9542 9.73316 6.02501C9.73772 6.09581 9.76322 6.16367 9.80642 6.21999C9.84407 6.27497 9.89537 6.31925 9.95529 6.34847C10.0152 6.3777 10.0817 6.39087 10.1483 6.3867C10.3883 6.38233 10.6301 6.38357 10.8745 6.38357Z" fill="#E2E2E2"/>
                                    <path d="M1.13994 5.61064C0.898075 5.61064 0.656213 5.61064 0.414977 5.61064C0.17374 5.61064 0 5.78172 0 5.99525C0.000697981 6.04791 0.0119899 6.09989 0.0332041 6.14811C0.0544183 6.19632 0.0851214 6.23977 0.123487 6.27589C0.161852 6.31201 0.207097 6.34004 0.256529 6.35834C0.30596 6.37663 0.35857 6.3848 0.411227 6.38237C0.894533 6.38695 1.37784 6.38695 1.86115 6.38237C1.96325 6.38287 2.06137 6.34282 2.13392 6.27104C2.20647 6.19926 2.2475 6.10163 2.248 5.99962C2.2485 5.89762 2.20841 5.79959 2.13657 5.72711C2.06472 5.65463 1.967 5.61363 1.8649 5.61313C1.62491 5.60627 1.3818 5.61064 1.13994 5.61064Z" fill="#E2E2E2"/>
                                </svg>
                            </div>
                            <div onClick={toggleTheme} style={{width:15,height:15,borderRadius:20,marginRight:3,marginTop:1}}>
                                <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                 <circle cx="7.5" cy="7.5" r="7.5" fill="#E2E2E2"/>
                                </svg>
                            </div>
                        </div>
                        <div style={{marginLeft:11}} className='ml-2'>
                             <p onClick={()=>cambioLingua()} style={{cursor:'pointer', fontSize:13, color:theme.colorT}}>{lang}</p>
                        </div>    
                        
                        </div>
                    ) : (
                        <div className={'flex'}>
                        <div style={{height:21,width:41, borderRadius:10, border:theme.border , display:'flex',alignItems: 'center',justifyContent: 'space-between'}}>
                            <div  style={{borderRadius:20,marginLeft:3,marginTop:1}}>
                                <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="7.5" cy="7.5" r="7.5" fill="#3E3E3E"/>
                                </svg>
                            </div>
                            <div  onClick={toggleTheme} style={{marginRight:3,cursor:'pointer'}}>
                              <svg width="10" height="11" viewBox="0 0 10 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M4.50321 0.698883C2.26523 1.45901 0.708375 3.67735 0.950757 6.06241C1.01904 6.727 1.22579 7.3672 1.55888 7.94542C1.89197 8.52364 2.34469 9.02824 2.89046 9.4296C3.43623 9.83096 4.06406 10.121 4.7371 10.2827C5.41014 10.4444 6.11482 10.4744 6.80979 10.3711C7.61639 10.2594 8.38959 9.96333 9.06343 9.50808C9.10576 9.4823 9.14594 9.45334 9.18357 9.42147C9.23163 9.38155 9.26814 9.33043 9.28931 9.27338C9.31049 9.21634 9.31556 9.15544 9.30402 9.09698C9.29248 9.03852 9.26473 8.98462 9.22365 8.94085C9.18257 8.89707 9.12964 8.86501 9.07033 8.84798C9.00306 8.82916 8.93461 8.81397 8.86531 8.80249C7.36036 8.45432 6.21702 7.64942 5.49302 6.34559C4.77515 5.05735 4.68515 3.69538 5.18825 2.27945C5.34053 1.85653 5.55296 1.45399 5.8194 1.08345C5.85183 1.03818 5.88641 0.993868 5.91489 0.945986C5.98761 0.826313 6.01724 0.701255 5.93687 0.580166C5.8994 0.518647 5.84237 0.470567 5.77376 0.442654C5.70515 0.414742 5.62838 0.408394 5.55422 0.4245C5.31647 0.467924 5.08035 0.521084 4.84621 0.579845C4.73001 0.613881 4.61552 0.653614 4.50321 0.698883ZM4.83598 1.29726C4.05166 2.86996 3.92566 4.4464 4.5513 6.03365C5.17694 7.6209 6.36008 8.67696 8.00409 9.28987C7.04134 9.82061 5.22984 10.0352 3.75328 9.14248C2.15406 8.17517 1.43905 6.74269 1.70491 4.91457C1.96472 3.12753 3.0907 1.95243 4.8349 1.29765L4.83598 1.29726Z" fill="#333333"/>
                              </svg>
                            </div>
                        </div>
                        <div style={{marginLeft:11}} className='ml-2'>
                             <p onClick={()=>cambioLingua()} style={{cursor:'pointer', fontSize:13, color:theme.colorT}}>{lang}</p>
                        </div> 
                        </div>
                    )}
                    
                    
                </div>
                <main className={allText ? 'onlytxt' : 'gridLay'}>
                <main>{children}</main>
                
                </main>
                
                <footer style={{background:' #e6e6e5',color:theme.colorT}} className='fotters'>
                    <div style={{alignItems: 'flex-end',gap: 26}} className='flex '>
                    {/*<p>SoundsOfThings Srl</p>*/}
                    <div style={{fontSize:12}}>
                        <strong>SoundsOfThings Srl</strong>
                        <p>Via scuderie 7, 39012 Merano (BZ) - Italia</p>
                        <p>Codice fiscale e partita IVA 14249081002</p>
                    </div>
                    <div style={{fontSize:12}}>
                        <p>Cap. Sociale <strong>€10.731,92</strong> i.v.</p>
                        <p>R.E.A. di Bolzano n. 220848</p>
                    </div>
                    <div style={{fontSize:12}}>
                        <strong>{lang != 'IT' ? 'Unità operativa' :  'Operating Unit'}</strong>
                        <p>Viale Principe Umberto 67/bis, 98122 Messina (ME) - Italia</p> 
                        <p>R.E.A. di Messina ME - 249302</p>
                    </div>
                    <div style={{fontSize:12}}>
                        {lang == 'IT' ? (
                        <a target={'_blank'} href='/privacy' rel="noreferrer"><strong>Privacy</strong></a>
                        ):(
                         <a target={'_blank'} href='/it/privacy' rel="noreferrer"><strong>Privacy</strong></a>
  
                        )}
                        <br></br>
                        {lang == 'IT' ? (
                        <a target={'_blank'} href='/t&c' rel="noreferrer"><strong>Terms and Conditions</strong></a>
                        ):(
                         <a target={'_blank'} href='/it/t&c' rel="noreferrer"><strong>Termini e Condizioni</strong></a>
  
                        )}
                        <br></br>
                        {lang != 'IT' ? (
                            // eslint-disable-next-line @next/next/no-html-link-for-pages
                            <a target={'_blank'} href='/it/r&d'><strong>R&D riconoscimenti e contributi</strong></a>
                        ) : (
                            // eslint-disable-next-line @next/next/no-html-link-for-pages
                            <a target={'_blank'} href='/r&d'><strong>R&D awards and grants</strong></a>
                        )}
                        <br></br>
                        <a href='#' class='iubenda-cs-preferences-link'>{lang != 'IT' ? 'Preferenze privacy' : 'Privacy preferences'}</a>

                    </div>
                    </div>
                    <div className='fottersItems'>
                    
                    <div style={{marginRight:0, display:'flex',gap:26,height:54,alignItems: 'flex-end'}}>
                        <div>
                            <a target='_blank' href='https://www.instagram.com/soundsofthings/' rel="noreferrer"><img style={{height:30,width:30}} src={theme.in}/></a>
                        </div>
                        <div>
                            <a  target='_blank' href='https://www.facebook.com/globalsoundvillage/' rel="noreferrer"><img style={{height:30,width:30}} src={theme.fb} /></a>
                        </div>
                        <div>
                            <a  target='_blank' href='https://twitter.com/SoundsOfThings' rel="noreferrer"><img style={{height:30,width:30}} src={theme.tw} /></a>
                        </div>
                        {/*<div>
                            <a  target='_blank' href=''><img style={{height:30,width:30}} src={theme.vm} /></a>
                        </div>*/}
                        <div>
                            <a  target='_blank' href='https://www.youtube.com/channel/UCyp56Ox_awg9N5xt8fn5axw' rel="noreferrer"><img style={{height:30,width:30}} src={theme.yt} /></a>
                        </div>
                        <div>
                            <a  target='_blank' href='https://t.me/soundsofthings' rel="noreferrer"><img style={{height:30,width:30}} src={theme.tk} /></a>
                        </div>
                     </div>
                     <div>
                        {/*<ul>
                            <li>privacy link</li>
                    </ul>*/}
                    </div>
                    </div>
                </footer>
            </div>
        )
    }
    
}

export default Layout;