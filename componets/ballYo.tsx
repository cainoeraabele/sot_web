import React, {useEffect, useState ,useRef} from "react";
import { useTheme ,themes } from '../lib/ThemeContext'
//import { bubble } from '../lib/object'
import { useRouter } from 'next/router'
import { GraphQLClient } from 'graphql-request'


interface props {
  mobile:string
}




function getRandomInt(min:number, max:number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min); // The maximum is exclusive and the minimum is inclusive
}

// return distance to given circle, in rem
function distanceTo(circle:any,oter:any) {
  const xDiff = circle.x - oter.x;
  const yDiff = circle.y - oter.y;

  
  return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
}

// return the collision distance to given circle, in rem
function collideDistance(circle:any,oter:any) {
  const sumRadius = circle.size/3 + oter.size //originalRadius * getScale() + circle.radius;
  const distance = distanceTo(circle,oter);
  return sumRadius - distance;
}

function collideTo(circle:any,oter:any) {
  return collideDistance(circle,oter) > 0;
}

function pxToRem(px:any) {
    return px / parseInt(getComputedStyle(document.body).getPropertyValue('font-size'));
}

function parsePx(px:any) {
    return Number(px.replace("px", ""));
}

function getContainerWidthInRem(container:any) {
  return parsePx(getComputedStyle(container).width);
}
function getContainerHeightInRem(container:any) {
  return parsePx(getComputedStyle(container).height);
}

export default function Ball({mobile}:props) {
    const theme:any  = useTheme().theme
    const router = useRouter()
    const ref = useRef(null);
    
  var timerx: string | number | NodeJS.Timeout | undefined;
  const [containerN , setContainerN ] = useState<any>(null)
  const [structure , setStructure ] = useState<any>([])
  const [move , _setMove ] = useState<boolean>(false)
  const [conter , setconter ] = useState<number>(0)
  const [itemsN , _setItemsN ] = useState<number | null>(null)
  const [active , _setActive ] = useState<boolean>(false)
  const [timer , _setTimer ] = useState<number | null>(null)
  const [initial, _setInitial] = useState<any>({
    initialX:null,
    initialY: null
  })
  const [oper,setOper] = useState<number>(0)
  const myStructureRef = React.useRef(structure);
  const myActiveeRef = React.useRef(active);
  const myItemsNeRef = React.useRef(itemsN);
  const myInitialeRef = React.useRef(initial);
  const myMoveeRef = React.useRef(move);


  const mytimeRef = React.useRef(timer);



  const setMove = (data:any) => {
    myMoveeRef.current = data;
    _setMove(data);
  };

  const setTimer = (data:any) => {
    mytimeRef.current = data;
    _setTimer(data);
  };

  const setInitial = (data:any) => {
    myInitialeRef.current = data;
    _setInitial(data);
  };
  
  const setItemsN = (data:any) => {
    myItemsNeRef.current = data;
    _setItemsN(data);
  };
  const setMyState = (data:any) => {
    myStructureRef.current = data;
    setStructure(data);
  };
  const setActive = (data:any) => {
    myActiveeRef.current = data;
    _setActive(data);
  };


  const test = async() =>{
    const query = `
      query {
        bubbles {
          id
          url
          image {
            id
            url
          }
          gif {
            id
            url
          }
          imageDark {
            id
            url
          }
          active
        }
      }
    `
    
    const graphcms = new GraphQLClient('https://eu-central-1-shared-euc1-02.cdn.hygraph.com/content/clbcqkw4b11tf01tactj840qx/master')
    const bubbles = await graphcms.request(query)
   
    console.log('vediamo');
    console.log(bubbles.bubbles);


    const conteiner:any = ref.current;
    console.log('vediamo');
   
    let hrt = parsePx(getComputedStyle(conteiner).width)
    console.log(hrt);
    
    let bubble:any = []
    
    bubbles.bubbles.map((x:any,i:number)=>{
      let isSpecial = false;
      let positions = {
        x:getRandomInt(-100,500),
        y:getRandomInt(-100,500)
      }
      let urlH = router.pathname

      console.log('vediamooooo');
      console.log(urlH);
      
      
      let arrr = urlH.split('/')
      let base = '/'
      if(arrr[1] == 'it'){
        base = '/it/'
      }

      let generateUrl = x.url == 'index' ? base : base+x.url
      let sizeB = getRandomInt(hrt/7,hrt/6)

      let presernt = false
      if(router.pathname == generateUrl){
        sizeB = hrt/2.8
        presernt = true
      }
      if(router.pathname === '/it' && generateUrl == '/it/'){
        sizeB = hrt/2.8
      }
      console.log('veiamo');
      console.log(x);
      
      
      if(x.active){
        bubble.push(
          {
            idB:'item'+i,
            title:'pollo',
            color:'yellow',
            size: sizeB,
            img:x.image.url,
            imgDR:x.imageDark.url,
            gif:x.gif ? x.gif.url : null,
            x:positions.x,
            y:positions.y,
            xOffset:positions.x,
            yOffset:positions.y,
            index:i,
            presernt:presernt,
            special:isSpecial,
            url:generateUrl
           }
        )
      }
      
    })
    
    /*
    if(router.pathname === '/'){
      bubble[0].size =  hrt/2.5 
      bubble[0].x=0
      bubble[0].y=0
      bubble[0].xOffset=0
      bubble[0].yOffset=0
      bubble[1].size = getRandomInt(hrt/6,hrt/5)
      bubble[2].size = getRandomInt(hrt/6,hrt/5)
      bubble[3].size = getRandomInt(hrt/6,hrt/5)
      bubble[4].size = getRandomInt(hrt/6,hrt/5)
      bubble[5].size = getRandomInt(hrt/6,hrt/5)
      bubble[6].size = getRandomInt(hrt/6,hrt/5)
    }
    if(router.pathname === '/sound_village_portal'){
      bubble[0].size = getRandomInt(hrt/6,hrt/5)
      bubble[1].size =  hrt/2.5 
      bubble[1].x=0
      bubble[1].y=0
      bubble[1].xOffset=0
      bubble[1].yOffset=0
      bubble[2].size = getRandomInt(hrt/6,hrt/5)
      bubble[3].size = getRandomInt(hrt/6,hrt/5)
      bubble[4].size = getRandomInt(hrt/6,hrt/5)
      bubble[5].size = getRandomInt(hrt/6,hrt/5)
      bubble[6].size = getRandomInt(hrt/6,hrt/5)
    }
    if(router.pathname === '/sound_logo'){
      bubble[0].size = getRandomInt(hrt/6,hrt/5)
      bubble[1].size = getRandomInt(hrt/6,hrt/5)
      bubble[2].size =  hrt/2.5 
      bubble[2].x=0
      bubble[2].y=0
      bubble[2].xOffset=0
      bubble[2].yOffset=0
      bubble[3].size = getRandomInt(hrt/6,hrt/5)
      bubble[4].size = getRandomInt(hrt/6,hrt/5)
      bubble[5].size = getRandomInt(hrt/6,hrt/5)
      bubble[6].size = getRandomInt(hrt/6,hrt/5)
    }
    if(router.pathname === '/token_website'){
      bubble[0].size = getRandomInt(hrt/6,hrt/5)
      bubble[1].size = getRandomInt(hrt/6,hrt/5)
      bubble[2].size = getRandomInt(hrt/6,hrt/5)
      bubble[3].size =  hrt/2.5 
      bubble[3].x=0
      bubble[3].y=0
      bubble[3].xOffset=0
      bubble[3].yOffset=0
      bubble[4].size = getRandomInt(hrt/6,hrt/5)
      bubble[5].size = getRandomInt(hrt/6,hrt/5)
      bubble[6].size = getRandomInt(hrt/6,hrt/5)
    }
    if(router.pathname === '/app'){
      bubble[0].size = getRandomInt(hrt/6,hrt/5)
      bubble[1].size = getRandomInt(hrt/6,hrt/5)
      bubble[2].size = getRandomInt(hrt/6,hrt/5)
      bubble[3].size = getRandomInt(hrt/6,hrt/5)
      bubble[4].size = hrt/2.5 
      bubble[4].x=0
      bubble[4].y=0
      bubble[4].xOffset=0
      bubble[4].yOffset=0
      bubble[5].size = getRandomInt(hrt/6,hrt/5)
      bubble[6].size = getRandomInt(hrt/6,hrt/5)
    }
    if(router.pathname === '/composer_portal'){
      bubble[0].size = getRandomInt(hrt/6,hrt/5)
      bubble[1].size = getRandomInt(hrt/6,hrt/5)
      bubble[2].size = getRandomInt(hrt/6,hrt/5)
      bubble[3].size = getRandomInt(hrt/6,hrt/5)
      bubble[4].size = getRandomInt(hrt/6,hrt/5)
      bubble[5].size =  hrt/2.5 
      bubble[5].x=0
      bubble[5].y=0
      bubble[5].xOffset=0
      bubble[5].yOffset=0
      bubble[6].size = getRandomInt(hrt/6,hrt/5)
    }
    if(router.pathname === '/contact'){
      bubble[0].size = getRandomInt(hrt/6,hrt/5)
      bubble[1].size = getRandomInt(hrt/6,hrt/5)
      bubble[2].size = getRandomInt(hrt/6,hrt/5)
      bubble[3].size = getRandomInt(hrt/6,hrt/5)
      bubble[4].size = getRandomInt(hrt/6,hrt/5)
      bubble[5].size = getRandomInt(hrt/6,hrt/5)
      bubble[6].size =  hrt/2.5 
      bubble[6].x=0
      bubble[6].y=0
      bubble[6].xOffset=0
      bubble[6].yOffset=0
    }*/
    

    setMyState(bubble)
    
    
  }

  useEffect(()=>{
    
    test()
    
    
  },[])

  
  
  const goTo = (e:any) =>{
    console.log(myMoveeRef.current);
    
    if(myMoveeRef.current){
      return
    }else{
      alert('nooooo')
    }
   
  }

  const dragStart = (e:any) =>{

    if(e.target.id.includes("item")){
        let idT:string = e.target.id.replace('item','')
    


    if(idT != null){


      if(e.type == 'touchstart'){
        let pointers = e.touches[0]
        setInitial({
          initialX:  pointers.screenX - myStructureRef.current[idT].x,
          initialY: pointers.screenY - myStructureRef.current[idT].y
      
      
      })
      }else{
        setInitial({
          initialX:  e.clientX - myStructureRef.current[idT].x,
          initialY: e.clientY - myStructureRef.current[idT].y
      
      
      })
      }
        
        //initialX = e.clientX - myStructureRef.current[myItemsNeRef.current].x
        //initialY = e.clientX - myStructureRef.current[myItemsNeRef.current].y

    }

    setItemsN(Number(idT))
    setActive(true)
      //console.log('moveee');
      //console.log(i);
      
      let oooo = [...myStructureRef.current]
      
      
      oooo[Number(idT)].move = true
      setStructure(oooo)
    }
    
  }

  const imgreg = (obj:any) =>{
    if(theme != themes.dark){
      return obj.img
    }else{
      return obj.imgDR
    }
  }

  const resolveCollision = () =>{
    let strt = [...structure]
    const collisionPairs = [];

    for (const circle of structure) {
      for (const otherCircle of structure) {
          if (circle.idB !== otherCircle.idB && collideTo(circle,otherCircle)) {

              //console.log('collidono');
              
              // Collision has occured
              collisionPairs.push([circle, otherCircle]);

              //console.log(collisionPairs);

              const distance = distanceTo(circle,otherCircle);
              const distanceOverlap = -0.1 * collideDistance(circle,otherCircle);
              
              
              if (circle.move) {
                // Displace Target Ball away from collision fully from unmovable circle
                otherCircle.x += 2 * distanceOverlap * (circle.x - otherCircle.x) / distance;
                otherCircle.y += 2 * distanceOverlap * (circle.y - otherCircle.y) / distance;
           
              } else if (otherCircle.move) {
                // Displace Current Ball away from collision fully from unmovable target
                circle.x -= 2 * distanceOverlap * (circle.x - otherCircle.x) / distance;
                circle.y -= 2 * distanceOverlap * (circle.y - otherCircle.y) / distance;
            } else {
                // Displace Current Ball away from collision
                circle.x -= distanceOverlap * (circle.x - otherCircle.x) / distance;
                circle.y -= distanceOverlap * (circle.y - otherCircle.y) / distance;

                // Displace Target Ball away from collision
                otherCircle.x += distanceOverlap * (circle.x - otherCircle.x) / distance;
                otherCircle.y += distanceOverlap * (circle.y - otherCircle.y) / distance;

               
              

            }

            strt[circle.index] = circle
            strt[otherCircle.index] = otherCircle

            setStructure(strt)
              
              //console.log('collision');
              //console.log(collisionPairs);
              // Distance between ball centers
              
          }
      }
  }

    //console.log('controllo');
    
  }

  const clickSp = () =>{

  }

  const handleResize = () =>{
    console.log('ridimensiono');
    console.log(structure);
    const conteiner:any = ref.current;
    let hrt = parsePx(getComputedStyle(conteiner).width)

    let urlH = router.pathname
      let arrr = urlH.split('/')
      let base = '/'
      if(arrr[1] == 'it'){
        base = '/it/'
      }

      
    let arrrB:any = [];
    let ssss:any = [...structure]
    console.log(ssss);
    
    structure.forEach( (bumble:any) => {
      let generateUrl = bumble.url == 'index' ? base : base+bumble.url
      let sizeB = getRandomInt(hrt/6,hrt/5)

      let presernt = false
      if(router.pathname == generateUrl){
        sizeB = hrt/2.8
        presernt = true
      }
      if(router.pathname === '/it' && generateUrl == '/it/'){
        sizeB = hrt/2.8
      }
      console.log(sizeB);
      
      ssss.size=sizeB
      
      arrrB.push(ssss)
    });
    
    setStructure(arrrB)
    
  }

  useEffect(() => {
    // Aggiungi un gestore eventi per l'evento resize
    window.addEventListener('resize', function(){
      clearTimeout(timerx);
      
      timerx = setTimeout(function() {
        setStructure([])
        test()
        // Codice da eseguire alla fine del ridimensionamento
        //handleResize()
      }, 250);
    });

    // Rimuovi il gestore eventi quando il componente viene dismount
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);


  useEffect(() => {
    const conteiner:any = ref.current;
    //console.log('conteiner');
    const gravity = {
      x: (getContainerWidthInRem(conteiner) / 2),
      y: (getContainerHeightInRem(conteiner) / 2)
    };

    

    const timer = setTimeout(() => {
      setconter(conter+1)
      let ohhj = [...structure]
      ohhj.forEach((element:any) => {
        
        if(!element.move){

          const xToGravity = gravity.x - element.x - (element.size / 2);
          const yToGravity = gravity.y - element.y - (element.size / 2);
          
          

          //element.x *= 0.98;
          //element.y *= 0.98;
          

          element.x += xToGravity / 200;
          element.y += yToGravity / 200;
          
          //element.x = element.x + 0.98 
          //element.y = element.y + 0.98

          
          
        }
        
        
      });
      resolveCollision()
      //setStructure(ohhj)
      //console.log('This will run after 1 second!')
    }, 1);
    
    return () => clearTimeout(timer);
  }, [conter]);


  useEffect(() => {
    const conteiner:any = ref.current;

    const gravity = {
      x: (getContainerWidthInRem(conteiner) / 2)*10,
      y: (getContainerHeightInRem(conteiner) / 2)*10
    };
    
    
    
    
    const handleClick = (event:any) => {
      
      
      console.log('Button clicked');
      
    };

    
    
    const element:any = ref.current;

    element.addEventListener('click', handleClick);

   
  
      function dragEnd(e:any) {
        e.stopPropagation();
        setActive(false)
        
        
        let oooo = [...myStructureRef.current]
        
        if(myItemsNeRef.current != null){
          oooo[myItemsNeRef.current].move = false

        }
        setStructure(oooo)
        
        if(myMoveeRef.current){
          //alert('hai mosso')
          setMove(false)
        }else{
          //alert('forse clcik')
          e.preventDefault()
          if(myItemsNeRef.current != null){
            router.push(oooo[myItemsNeRef.current].url)
          }
          
        }
        
      }
  
      function drag(e:any) {
        
        
        if(myActiveeRef.current){
          console.log('drag');
          setMove(true)
            let oldDtruct = [...myStructureRef.current]
            
            if(e.type == 'touchmove'){
              let pointers = e.touches[0]
              if(myItemsNeRef.current != null){
                
                
                oldDtruct[myItemsNeRef.current].x = pointers.screenX -  myInitialeRef.current.initialX
                oldDtruct[myItemsNeRef.current].y = pointers.screenY -   myInitialeRef.current.initialY

                 setMyState(oldDtruct)
            }
              
            }else{
              if(myItemsNeRef.current != null){
                oldDtruct[myItemsNeRef.current].x = e.clientX -  myInitialeRef.current.initialX
                oldDtruct[myItemsNeRef.current].y = e.clientY -   myInitialeRef.current.initialY

                 setMyState(oldDtruct)
            }
            }
            
            
            
        
        }
         
         
        
      }

    element.addEventListener("touchstart", dragStart, false);
    element.addEventListener("touchend", dragEnd, false);
    element.addEventListener("touchmove", drag, false);
    element.addEventListener("mousedown", dragStart, false);
    element.addEventListener("mouseup", dragEnd, false);
    element.addEventListener("mousemove", drag, false);

    return () => {
      element.removeEventListener("touchstart", dragStart, false);
      element.removeEventListener("touchend", dragEnd, false);
      element.removeEventListener("touchmove", drag, false);
      element.removeEventListener("mousedown", dragStart, false);
      element.removeEventListener("mouseup", dragEnd, false);
      element.removeEventListener("mousemove", drag, false);
    };
  }, []);



  if(mobile){
    
    return (
      <div>
        
        <div ref={ref} style={{backgroundColor:theme.background, marginLeft:0,height:'100vw' }} id="containerz">
        {structure.map((x:any,i:number)=>{
             if(x.presernt){
              return(
                <>
                <div
                 style={{backgroundImage:x.img  ? 'url('+imgreg(x)+')' : '#fff' , width:x.size,height:x.size, transform: "translate3d(" + x.x + "px, " + x.y + "px, 0)",transition: "width 250ms linear 0s , height 250ms linear 0s",}} key={i} className="itemee ytnt "  id={"item"+i}>
                </div>
                </>)
             }else{
              if(x.url === '/cta' || x.url === '/it/cta' ){
                return(
                <div 
                 style={{backgroundImage:x.img  ? 'url('+imgreg(x)+')' : '#fff', width:x.size,height:x.size, transform: "translate3d(" + x.x + "px, " + x.y + "px, 0)",transition: "width 250ms linear 0s , height 250ms linear 0s",}} key={i} className="itemee testacio"  id={"item"+i}>
                  {/*<img className="testacio" src="/gif/CTA.gif" style={{height:x.size,width:x.size,position:'absolute'}} />*/}

                </div>
              )
              }else{
                return(
                  <div 
                   style={{backgroundImage:x.img  ? 'url('+imgreg(x)+')' : '#fff', width:x.size,height:x.size, transform: "translate3d(" + x.x + "px, " + x.y + "px, 0)",transition: "width 250ms linear 0s , height 250ms linear 0s",}} key={i} className="itemee"  id={"item"+i}>
                    <p style={{color:'#000'}}></p>
                  </div>
                )
              }
              
             }
            
          })}
        </div>
        
        
      </div>
    )
  }else{
    return (
      <div>
        
        <div  ref={ref} style={{backgroundColor:theme.background, marginLeft:0,height:mobile ? '20vh' :  '100vh',marginTop:mobile ? 70 : 0,maxWidth:mobile ? '100vw' : 'auto'  }} id="containerz">
          {structure.map((x:any,i:number)=>{
             if(x.presernt){
              return(
                <>
                <div
                 style={{backgroundImage:x.img  ? 'url('+imgreg(x)+')' : '#fff' , width:x.size,height:x.size, transform: "translate3d(" + x.x + "px, " + x.y + "px, 0)",transition: "width 250ms linear 0s , height 250ms linear 0s",}} key={i} className="itemee ytnt "  id={"item"+i}>
                </div>
                </>)
             }else{
              if(x.url === '/cta' || x.url === '/it/cta' ){
                return(
                <div 
                 style={{backgroundImage:x.img  ? 'url('+imgreg(x)+')' : '#fff', width:x.size,height:x.size, transform: "translate3d(" + x.x + "px, " + x.y + "px, 0)",transition: "width 250ms linear 0s , height 250ms linear 0s",}} key={i} className="itemee testacio"  id={"item"+i}>
                  {/*<img className="testacio" src="/gif/CTA.gif" style={{height:x.size,width:x.size,position:'absolute'}} />*/}

                </div>
              )
              }else{
                return(
                  <div 
                   style={{backgroundImage:x.img  ? 'url('+imgreg(x)+')' : '#fff', width:x.size,height:x.size, transform: "translate3d(" + x.x + "px, " + x.y + "px, 0)",transition: "width 250ms linear 0s , height 250ms linear 0s",}} key={i} className="itemee"  id={"item"+i}>
                    <p style={{color:'#000'}}></p>
                  </div>
                )
              }
              
             }
            
          })}
        </div>
        
        
      </div>
    )
  }
  
}


