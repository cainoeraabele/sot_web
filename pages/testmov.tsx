import React, {useEffect, useState} from "react";
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

export default function Home() {

  let bubble = [
    {
     title:'pollo',
     size:100,
     x:10,
     y:100,
     xOffset:10,
     yOffset:100,
     index:0
    },
    {
      title:'pixxxa',
      size:200,
      x:100,
      y:200,
      xOffset:100,
      yOffset:200,
      index:1
     },
     {
      title:'pollo',
      size:100,
      x:10,
      y:400,
      xOffset:10,
      yOffset:100,
      index:2
     },
     {
       title:'pixxxa',
       size:200,
       x:400,
       y:200,
       xOffset:100,
       yOffset:200,
       index:3
      }
  ]

  const [containerN , setContainerN ] = useState<any>(null)
  const [structure , setStructure ] = useState<any>([])
  const [move , setMove ] = useState<boolean>(false)
  
  useEffect(()=>{
    setStructure(bubble)
  },[])

  useEffect(()=>{


    //controllCrachNew()

    var dragItem:any = document.querySelectorAll(".itemee");
    //console.log(dragItem);
    var container:any = document.querySelector("#container");
    let offsettre = container.getBoundingClientRect()
    setContainerN(container.getBoundingClientRect())
    //console.log('info conteinew');
    
    //console.log(container.getBoundingClientRect());
    //console.log(container.offsetHeight);
    
    


    var active = false;
    var itemsN: any = null
    var currentX;
    var currentY;
    var initialX: any;
    var initialY: any;
    /*var xOffsetA:any = {
      0:10,
      1:100,
      2:0,
      3:0,
      4:0,
      5:0,
      6:0
    }
    var yOffsetA:any = {
      0:100,
      1:200,
      2:0,
      3:130,
      4:0,
      5:0,
      6:0
    }
    var xOffset = 0;
    var yOffset = 0;*/


    container.addEventListener("touchstart", dragStart, false);
    container.addEventListener("touchend", dragEnd, false);
    container.addEventListener("touchmove", drag, false);

    container.addEventListener("mousedown", dragStart, false);
    container.addEventListener("mouseup", dragEnd, false);
    container.addEventListener("mousemove", drag, false);

    function dragStart(e:any) {
      //xOffsetA[itemsN] = currentX;
      //yOffsetA[itemsN] = currentY;

      console.log('sss');
      
      /*dragItem.forEach((item:any,i:number) => {
        if (e.target === item) {

          //console.log(e);
          itemsN = i
          active = true;
          setMove(true)
        }
      });*/

      if (e.type === "touchstart") {
      } else {

        if( structure[itemsN]){
          initialX = e.clientX - structure[itemsN].x ;
          initialY = e.clientY - structure[itemsN].y;
        }
       
      }




    }

    function dragEnd(e:any) {
      //console.log('fine');
      //console.log(xOffsetA);
      //console.log(yOffsetA);
      initialX = null;
      initialY = null;
      itemsN = null
      active = false;
      setMove(false)
    }

    function drag(e:any) {
      if (active) {

        e.preventDefault();

        if (e.type === "touchmove") {
          //console.log('move');
          currentX = e.touches[0].clientX - initialX;
          currentY = e.touches[0].clientY - initialY;
        } else {
          //console.log('Bomove');
          currentX = e.clientX - initialX;
          currentY = e.clientY - initialY;
        }

         if( currentX > (offsettre.left ) && currentY > (offsettre.top ) &&  currentX < (offsettre.right - 100) && currentY < (offsettre.bottom - 100)){
        
        let oldDtruct = [...structure]
        oldDtruct[itemsN].x = currentX
        oldDtruct[itemsN].y = currentY
        

        //controllCrach(itemsN,oldDtruct[itemsN])  
        setStructure(oldDtruct)
        //controllCrachNew()
        

         // setTranslate( oldDtruct[itemsN].x,  oldDtruct[itemsN].y, dragItem[itemsN]);
        }

       
      }
    }

  },[structure])
  

  const controllCrach = (index:number,posix:any) =>{
    let struttureee = [...structure]
    /*console.log(struttureee);
    console.log('pos');
    console.log(posix);*/
    let top = posix.y
    let left = posix.x
    let right = posix.x + posix.size
    let botton = posix.y + posix.size

   // console.log('pppp qq');
    
   // console.log(right);
   // console.log('_____');
    
    

    struttureee.forEach((babo:any,i:number) => {
      let topB = babo.y
      let leftB = babo.x
      let rightB = babo.x + babo.size
      let bottonB = babo.y + babo.size
      if(index != i){
       // console.log(babo.title+index+'_'+leftB);
        if(
          right >= leftB &&
          left <= rightB &&
          botton >= topB &&
          top <=  bottonB
        ){
          console.log('okkkkk');
          //console.log('toccato '+babo.title+i+' da '+posix.title+index);
          let stru  = [...structure]
          stru[i].x = stru[i].x-1
          stru[i].y = stru[i].y-1
          setStructure(stru)
          
          
        }
      }
     

    });
    
    
  }

  const controllCrachNew = () =>{
    let struttureee = [...structure]
    /*console.log(struttureee);
    console.log('pos');
    console.log(posix);*/
    
    
    

   
    struttureee.every((e:any,index:number)=>{

      let top = e.y
      let left = e.x
      let right = e.x + e.size
      let botton = e.y + e.size
      
      struttureee.forEach((babo:any,i:number) => {
        let topB = babo.y
        let leftB = babo.x
        let rightB = babo.x + babo.size
        let bottonB = babo.y + babo.size
        if(index != i){
         // console.log(babo.title+index+'_'+leftB);
          if(
            right >= leftB &&
            left <= rightB &&
            botton >= topB &&
            top <=  bottonB
          ){
            
           // console.log('toccato '+babo.title+i+' da '+posix.title+index);
            let stru  = [...structure];
            stru[i].size =  stru[i].size - 1
            //stru[i].x = stru[i].x-1
            //stru[i].y = stru[i].y-1
            setStructure(stru)
            
            
          }
        }
       
  
      });
      
    })
    
    
  }

  const test = () =>{
    let sss = [...structure]
    sss[0].size = 300; 
    sss[1].size = 100; 
    sss[1].x = 300 
    setStructure(sss)
  }
  const hhh = ()=>{
    if(!move){
      //alert('ppp')
    }

  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
       
      </Head>

      <div style={{marginLeft:0}} id="container">
        <button onClick={()=>test()}>test</button>
        {/*<div  className="itemee" id="item">

        </div>
        <div className="itemee" id="item2">

        </div>
        <div className="itemee" id="item3">

        </div>
        <div className="itemee" id="item4">

        </div>
        <div className="itemee" id="item5">

        </div>
        <div className="itemee" id="item6">

        </div>
        <div className="itemee" id="item7">

        </div>*/}
        {structure.map((x:any,i:number)=>{
          return(
            <div
             style={{width:x.size,height:x.size, transform: "translate3d(" + x.x + "px, " + x.y + "px, 0)",transition: "width 250ms linear 0s , height 250ms linear 0s",}} key={i} className="itemee" id={"item"+i}>
              <p style={{color:'#000'}}>{x.title} {i}</p>
            </div>
          )
        })}
      </div>
      <div>
        {JSON.stringify(structure)}
      </div>
      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  )
}
