import { useEffect, useState } from 'react'
import { useTheme , themes } from '../lib/ThemeContext'
import Layout from '../componets/layoutY'
import { GraphQLClient } from 'graphql-request'
import { isMobile } from 'react-device-detect';
import Script from 'next/script'


const graphcms = new GraphQLClient('https://eu-central-1-shared-euc1-02.cdn.hygraph.com/content/clbcqkw4b11tf01tactj840qx/master')

export default function Home({}) {
  const { theme, setTheme } = useTheme()
  const [ mobileV, setMobilev ] = useState(null)
  const [ sezione, setSezione ] = useState(null)


  const toggleTheme = () => {
    setTheme(theme === themes.dark ? themes.light : themes.dark)
  }


  // Query Data
const call = async() =>{
  try {
    const query = `
      {
        pagineExtra(where: {id: "cldart5a70i0x0avyhi1pmrn3"}) {
          testo{
            html
          }
        }
      }
    `
    
    const { pagineExtra}= await graphcms.request(query)

    
    setSezione(pagineExtra)
  } catch (error) {
    console.log(error)
  }
}

  useEffect(()=>{
    call()
  },[])

  useEffect(()=>{
    setMobilev(isMobile)
  },isMobile)

  if(mobileV){
    return (
      <Layout mobile>
      <main className='faddd'>
       
      </main>
      </Layout>
    )
  }else{
    return (
      <Layout>
        <main style={{height: '100%', display: 'grid',alignContent: 'space-between'}} className=''>
      
      <iframe style={{marginTop:70, height:'calc(100vh - 150px)',width:'100vw'}} src="/pynen.html" title="W3Schools Free Online Web Tutorials"></iframe>

      </main>
    </Layout>
    )
  } 

}