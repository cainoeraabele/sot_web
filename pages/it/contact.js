import { useEffect, useState } from 'react'
import { useTheme , themes } from '../../lib/ThemeContext'
import Layout from '../../componets/layout'
import { isMobile } from 'react-device-detect';
import { Formik, Field, Form ,errors } from 'formik';
import { GraphQLClient } from 'graphql-request'

import Link from 'next/link'
import * as Yup from "yup";

const graphcms = new GraphQLClient('https://eu-central-1-shared-euc1-02.cdn.hygraph.com/content/clbcqkw4b11tf01tactj840qx/master')


export default function Home({}) {
  const { theme, setTheme } = useTheme()
  const [ mobileV, setMobilev ] = useState(null)


  const toggleTheme = () => {
    setTheme(theme === themes.dark ? themes.light : themes.dark)
  }

  useEffect(()=>{
    setMobilev(isMobile)
  },isMobile)


  const inviaEmail = (objUserTo) =>{
    const axios = require('axios');
    //    axios.get('/api/email?nome='+objUserTo.Firstname+'&cognome='+objUserTo.Lastname+'&email='+objUserTo.Email , {

    axios.get('/api/email?nome='+objUserTo.firstName+'&cognome='+objUserTo.lastName+'&email='+objUserTo.email+'&mess='+objUserTo.text, {
      headers: {"Access-Control-Allow-Origin": "*"}
    })
    .then(function (response) {
      // handle success
      console.log('vvvvvv');
      console.log(response);
      //alert('inviata')
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
    .then(function () {
      
      // always executed
    });
  }

  if(mobileV){
    return (
      <Layout mobile>
      <main className='faddd'>
         <div>
            <h1 style={{fontSize:41,marginTop:0,color:theme.colorT}}>Form di contatto</h1>
            <h4 style={{color:theme.colorT}}>Inviaci un messaggio, siamo in ascolto:</h4>
          </div>
        <div style={{background:theme.background,position:'relative', color:theme.colorT,marginBottom:65}}>
        <Formik
      initialValues={{
        firstName: '',
        lastName: '',
        email: '',
        text: '',
        privacy: false
      }}
      onSubmit={async (values) => {
        await new Promise((r) => setTimeout(r, 500));
        inviaEmail(values)
        alert('Messaggio Inviato');
      }}
      validationSchema={Yup.object().shape({
          email: Yup.string()
            .email()
            .required("Email Required"),
          firstName: Yup.string()
            .required("First Name Required"),
          lastName: Yup.string()
            .required("Last Name Required"),
          text: Yup.string()
          .required("Message Required"),
          privacy: Yup.bool().oneOf([true],"l'accettazione della Privacy Policy è obbligatoria"),
        })}
    >
      {props => {
        const {
          values,
          touched,
          errors,
          isSubmitting,
          resetForm,
          handleChange,
          handleBlur,
          handleSubmit
        } = props;
        return (
          <>
            <Form>
        <div className='formm' style={{marginTop:20,maxWidth:400, fontSize:18}}>
        {errors.firstName && touched.firstName && (
                <div className="input feedback">{errors.firstName}</div>
              )}
        <div style={{borderBottom:'1px solid', display:'flex'}}>
            <div style={{width:170}}>
              <p>NOME*</p>
            </div>
            <div>
              <Field id="firstName" name="firstName" placeholder="Jane" />
            </div>
          </div>
          {errors.lastName && touched.lastName && (
                <div className="input feedback">{errors.lastName}</div>
              )}
          <div style={{borderBottom:'1px solid',display:'flex'}}>
          <div style={{width:170}}>
              <p>COGNOME*</p>
            </div>
            <div>
              <Field id="lastName" name="lastName" placeholder="Doe" />
            </div>
          </div>
           {errors.email && touched.email && (
                <div className="input feedback">{errors.email}</div>
              )}
          <div style={{borderBottom:'1px solid',display:'flex'}}>
            <div style={{width:170}}>  
              <p>EMAIL*</p>
            </div>
            <div>
            <Field
              id="email"
              name="email"
              placeholder="jane@acme.com"
              type="email"
            />
            </div>
          </div>
          {errors.text && touched.text && (
                <div className="input feedback">{errors.text}</div>
              )}
          <div style={{borderBottom:'1px solid',}}>
            <div>
              <p>MESSAGGIO*</p>
            </div>
            <div>
              <Field
                as='textarea'
                id="text"
                name="text"
                placeholder='...'
                style={{width:'100%',background:'transparent'}}
              />
            </div>
          </div>
          <br></br>
          {errors.privacy && touched.privacy && (
                <div className="input feedback">{errors.privacy}</div>
              )}
          <label style={{fontSize:14,paddingLeft:5}}>
            <Field style={{marginRight:10}} type="checkbox" name="privacy"  />
            Accetto le condizioni della <Link  style={{fontWeight:'bold',color:'#000'}} target={'_blank'} href='/it/privacy'>PRIVACY</Link>
          </label>
          <br></br>
          <br></br>
          <button disabled={JSON.stringify(errors) != '{}'} style={{opacity: JSON.stringify(errors) != '{}' ? 0.5 : 1, border: '1px solid #3E3E3E',filter: 'drop-shadow(0px 1px 1px rgba(0, 0, 0, 0.25))',padding:5, paddingLeft:20,paddingRight:20}} type="submit"><p style={{color:'#333'}}>INVIA</p></button>
        </div>
      </Form>
          </>
        );
      }}
    </Formik>
            
        </div>
      </main>
      </Layout>
    )
  }else{
    return (
      <Layout>
        <main style={{height: '100%', display: 'grid',alignContent: 'space-between'}} className='faddd'>
          <div>
            <h1 style={{fontSize:41,marginTop:100,color:theme.colorT}}>Contact</h1> 
            <h4 style={{color:theme.colorT}}>Inviaci un messaggio, siamo in ascolto:</h4>
   
          </div>
          <div style={{height:'60vh',marginBottom:65}}>
          <div style={{overflow:'scroll',height:'60vh',color:theme.colorT,paddingBottom: 70}}>
          <Formik
      initialValues={{
        firstName: '',
        lastName: '',
        email: '',
        text: '',
        privacy: false
      }}
      onSubmit={async (values) => {
        await new Promise((r) => setTimeout(r, 500));
        inviaEmail(values)
        alert('Messaggio Inviato');
      }}
      validationSchema={Yup.object().shape({
          email: Yup.string()
            .email()
            .required("Email Obbligatiria"),
          firstName: Yup.string()
            .required("Nome Obbligatirio"),
          lastName: Yup.string()
            .required("Cognome Obbligatiria"),
          text: Yup.string()
          .required("Messaggio Obbligatiria"),
          privacy: Yup.bool().oneOf([true],"l'accettazione della Privacy Policy è obbligatoria"),
        })}
    >
      {props => {
        const {
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit
        } = props;
        return (
          <>
            <Form>
        <div className='formm' style={{marginTop:20,maxWidth:400, fontSize:18}}>
        {errors.firstName && touched.firstName && (
                <div className="input feedback">{errors.firstName}</div>
              )}
        <div style={{borderBottom:'1px solid', display:'flex'}}>
            <div style={{width:170}}>
              <p>NOME*</p>
            </div>
            <div>
              <Field id="firstName" name="firstName" placeholder="Jane" />
            </div>
          </div>
          {errors.lastName && touched.lastName && (
                <div className="input feedback">{errors.lastName}</div>
              )}
          <div style={{borderBottom:'1px solid',display:'flex'}}>
          <div style={{width:170}}>
              <p>COGNOME*</p>
            </div>
            <div>
              <Field id="lastName" name="lastName" placeholder="Doe" />
            </div>
          </div>
           {errors.email && touched.email && (
                <div className="input feedback">{errors.email}</div>
              )}
          <div style={{borderBottom:'1px solid',display:'flex'}}>
            <div style={{width:170}}>  
              <p>EMAIL*</p>
            </div>
            <div>
            <Field
              id="email"
              name="email"
              placeholder="jane@acme.com"
              type="email"
            />
            </div>
          </div>
          {errors.text && touched.text && (
                <div className="input feedback">{errors.text}</div>
              )}
          <div style={{borderBottom:'1px solid',}}>
            <div>
              <p>MESSAGGIO*</p>
            </div>
            <div>
              <Field
                as='textarea'
                id="text"
                name="text"
                placeholder='...'
                style={{width:'100%',background:'transparent'}}
              />
            </div>
          </div>
          <br></br>
          {errors.privacy && touched.privacy && (
                <div className="input feedback">{errors.privacy}</div>
              )}
          <label style={{fontSize:14,paddingLeft:5}}>
            <Field style={{marginRight:10}} type="checkbox" name="privacy"  />
            Accetto le condizioni della <Link  style={{fontWeight:'bold',color:'#000'}} target={'_blank'} href='/it/privacy'>PRIVACY</Link>
          </label>
          <br></br>
          <br></br>
          <button disabled={JSON.stringify(errors) != '{}'} style={{opacity: JSON.stringify(errors) != '{}' ? 0.5 : 1, border: '1px solid #3E3E3E',filter: 'drop-shadow(0px 1px 1px rgba(0, 0, 0, 0.25))',padding:5, paddingLeft:20,paddingRight:20}} type="submit"><p style={{color:'#333'}}>INVIA</p></button>
        </div>
      </Form>
          </>
        );
      }}
    </Formik>
          </div>
          <div style={{backgroundColor:theme.colorBBB}} className='shadowns'></div>
        </div>
        </main>
      </Layout>
    )
  } 


}
