import { useState, useEffect } from 'react';

const GRAVITATIONAL_CONSTANT = 6.67408e-11; // Costante di gravità di Newton

export default function Gravity() {
  const [objects, setObjects] = useState([{
    mass: 1,
    position: { x: 100, y: 100 },
    velocity: { x: 0, y: 0 }
  }, {
    mass: 2,
    position: { x: 200, y: 200 },
    velocity: { x: 0, y: 0 }
  }]);

  useEffect(() => {
    console.log('pppp');
    let previousTime = null;
    function animate(time) {
      if (previousTime != null) {
        const deltaTime = (time - previousTime) / 1000; // Tempo in secondi
        updatePositions(deltaTime);
      }
      previousTime = time;

      requestAnimationFrame(animate);
    }
    requestAnimationFrame(animate);
  }, []);

  function updatePositions(deltaTime) {
    setObjects(prevObjects => {
      const updatedObjects = prevObjects.map(object => {
        return prevObjects.reduce((updatedObject, otherObject) => {
          if (object === otherObject) return updatedObject;

          const dx = otherObject.position.x - object.position.x;
          const dy = otherObject.position.y - object.position.y;
          const distance = Math.sqrt(dx * dx + dy * dy);

          // Calcola la forza di gravità in base alla massa e alla distanza
          const force = GRAVITATIONAL_CONSTANT * object.mass * otherObject.mass / (distance * distance);
          const acceleration = force / object.mass;

          // Aggiorna la velocità in base all'accelerazione
          updatedObject.velocity.x += acceleration * deltaTime * dx / distance;
          updatedObject.velocity.y += acceleration * deltaTime * dy / distance;

          return updatedObject;
        }, { ...object });
      });

      // Aggiorna la posizione in base alla velocità
      return updatedObjects.map(object => ({
        ...object,
        position: {
          x: object.position.x + object.velocity.x * deltaTime,
          y: object.position.y + object.velocity.y * deltaTime
        }
      }));
    });
  }

  return (
    <div>
      {objects.map(object => (
        <div
          key={object.mass}
          style={{
            position: 'absolute',
            left: object.position.x,
            top: object.position.y
          }}
        >
          Elemento
        </div>
      ))}
    </div>
  );
}
