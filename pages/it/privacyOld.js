import { useEffect, useState } from 'react'
import { useTheme , themes } from '../../lib/ThemeContext'
import Layout from '../../componets/layout'
import { GraphQLClient } from 'graphql-request'
import { isMobile } from 'react-device-detect';


const graphcms = new GraphQLClient('https://eu-central-1-shared-euc1-02.cdn.hygraph.com/content/clbcqkw4b11tf01tactj840qx/master')

export default function Home({}) {
  const { theme, setTheme } = useTheme()
  const [ mobileV, setMobilev ] = useState(null)
  const [ sezione, setSezione ] = useState(null)


  const toggleTheme = () => {
    setTheme(theme === themes.dark ? themes.light : themes.dark)
  }


  // Query Data
const call = async() =>{
  try {
    const query = `
      {
        pagineExtra(where: {id: "cldxgu84c06zb0auujneyitxz"}) {
          testoIta{
            html
          }
        }
      }
    `
    
    const { pagineExtra}= await graphcms.request(query)

    
    setSezione(pagineExtra)
  } catch (error) {
    console.log(error)
  }
}

  useEffect(()=>{
    call()
  },[])

  useEffect(()=>{
    setMobilev(isMobile)
  },isMobile)

  if(mobileV){
    return (
      <Layout mobile allText>
      <main className='faddd'>
        <div>
          <h1 style={{fontSize:31,marginTop:0,color:theme.colorT}}>Privacy Policy di www.soundsofthings.com</h1>
          
          
        </div>
        <div style={{background:theme.background,position:'relative', color:theme.colorT,marginBottom:65}}>
          <div className='Size20 boxRender' dangerouslySetInnerHTML={{__html: sezione?.testoIta?.html}} />
        </div>
      </main>
      </Layout>
    )
  }else{
    return (
      <Layout allText>
      <main style={{height: '100%', display: 'grid',alignContent: 'space-between'}} className='faddd'>
        <div>
          <h1 style={{fontSize:41,marginTop:100,color:theme.colorT}}>Privacy Policy di www.soundsofthings.com</h1>
          
          
        </div>
        <div style={{height:'50vh',marginBottom:65}}>
        <div style={{overflow:'scroll',height:'50vh',color:theme.colorT,paddingBottom:50}}>
          <div className='Size20 boxRender' dangerouslySetInnerHTML={{__html: sezione?.testoIta?.html}} />
          
        </div>
        <div style={{backgroundColor:theme.colorBBB}} className='shadowns'></div>

        </div>
      </main>
    </Layout>
    )
  } 

}