import { useTheme , themes } from '../../lib/ThemeContext'
import Layout from '../../componets/layout'
import { GraphQLClient } from 'graphql-request'
import { useEffect, useState } from 'react'
import ReactAudioPlayer from 'react-audio-player';
import { isMobile } from 'react-device-detect';

const graphcms = new GraphQLClient('https://eu-central-1-shared-euc1-02.cdn.hygraph.com/content/clbcqkw4b11tf01tactj840qx/master')

export default function Home({}) {
  const { theme, setTheme } = useTheme()
  const [sound,setSound] = useState([])
  const [ mobileV, setMobilev ] = useState(null)
  const [audio,setAudio] = useState([])
  const [selected,setSelected] = useState([])
  const [ sezione, setSezione ] = useState(null)
  const [ plll, setPll ] = useState(false)



  // Query Data
const call = async() =>{
try {
  const query = `
  {
    sezione(where: {id: "clbczh55k1lcv0cw3wp7v1hpv"}) {
      testoIt {
        html
      }
      titolo
      bottoni {
        ... on Bottone {
          testo
          url
        }
      }
    }
  }
`
   
 const { sezione }= await graphcms.request(query)

 console.log(sezione);
 setSezione(sezione)
} catch (error) {
 console.log(error)
}
}

useEffect(()=>{
 call()
},[])




  const toggleTheme = () => {
    setTheme(theme === themes.dark ? themes.light : themes.dark)
  }
  useEffect(()=>{
    setMobilev(isMobile)
  },isMobile)

  const soundList = async() =>{
    const query = `
      query {
        soundLogoElements {
          id
          titolo
          sound{
            url
          }
        }
      }
    `
    
    const sounds = await graphcms.request(query)

    console.log('vediamo');
    console.log(sounds.soundLogoElements);
    setSound(sounds.soundLogoElements)
    let attS = []
    sounds.soundLogoElements.forEach(element => {
      attS.push(new Audio(element.sound.url));
    });
    setAudio(attS)


    
    
  }

  const selectAudio = (i)=>{
    let sss = [...selected]
    let index = sss.indexOf(i);
    console.log('vediamo');
    if(index > -1){
      sss.splice(index, 1);
    }else{
      sss.push(i)
    }
    console.log('bohhhh');
    console.log(sss);
    setSelected(sss)
  }

  const playss = () =>{
    console.log('pppp');
    selected.forEach(element => {
      audio[element].play()
    });
    setPll(true)
  }
  const stop = () =>{
    console.log('pppp');
    selected.forEach(element => {
      audio[element].pause();
      audio[element].currentTime = 0;
    });
    setPll(false)
  }

  useEffect(()=>{
    soundList()
  },[])

  useEffect(()=>{
    if(plll){
      setTimeout(() => {
        setPll(false)
    }, 2100);
    }
  },[plll])

  const selecteedd = (i) =>{
    let sss = [...selected]
    let index = sss.indexOf(i);
    if(index > -1){
      return true
    }else{
      return false
    }
  }

  
  if(mobileV){
    return (
      <Layout mobile>
      <main className='faddd'>
         <div>
            <h1 style={{fontSize:41,marginTop:0,color:theme.colorT}}>{sezione?.titolo}</h1>
            <div className='flex gap-2'>
            {sezione?.bottoni?.map((m,i)=>(
              <div key={'button'+i} style={{color:theme.colorT}}  className='bLink'>
                <button><a target={'_blank'} href={m.url} rel="noreferrer">{m.testo}</a></button>
              </div>
            ))}
          </div>
          </div>
        <div style={{background:theme.background,position:'relative', color:theme.colorT,marginBottom:65}}>
        <div className='Size20 boxRender' dangerouslySetInnerHTML={{__html: sezione?.testoIt?.html}} />
            <div className='p-0 mt-10'>
            {sound.map((s,i)=>{
                  return(
                    <>
                    <div onClick={()=>{selectAudio(i)}} style={{cursor:'pointer',alignItems: 'center'}} className='flex gap-2' key={'sound_'+i}>
                      <div style={{width:15,height:15,border:'1px solid #000',borderColor:theme.colorT,borderRadius:2,background:selecteedd(i) ? '#000' : 'transparent'}}></div>
                      <p style={{fontSize:12}}>{s.titolo}</p>
                      <img style={{height:20, maxWidth:'74%'}} src={theme.sound} />
                    </div>
                    <hr style={{borderColor: theme.colorT}} className='mb-2 mt-2'></hr>
                    </>
                  )
                })}
                {plll ? (
                  <div onClick={()=>{stop()}} style={{cursor:'pointer',alignItems: 'center', justifyContent: 'space-around',marginBottom:10}} className='flex gap-2' >
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M2 2h20v20h-20z"/></svg>
                  </div>
                ) : (
                  <div onClick={()=>{ selected.length > 0 ? playss() : console.log('no')}} style={{cursor:'pointer',alignItems: 'center', justifyContent: 'space-around',marginBottom:10}} className='flex gap-2' >
                  <svg fill={selected.length > 0 ? '#000' : '#c4c0c9'} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M3 22v-20l18 10-18 10z"/></svg>
                  </div>
                )}
            </div>
        </div>
      </main>
      </Layout>
    )
  }else{
    return (
      <Layout>
      <main style={{height: '100%', display: 'grid',alignContent: 'space-between'}} className='faddd'>
          <div>
            <h1 style={{fontSize:41,marginTop:100,color:theme.colorT}}>{sezione?.titolo}</h1>
            <div className='flex gap-2'>
            {sezione?.bottoni?.map((m,i)=>(
              <div key={'button'+i} style={{color:theme.colorT}}  className='bLink'>
                <button><a target={'_blank'} href={m.url} rel="noreferrer">{m.testo}</a></button>
              </div>
            ))}
          </div>
          </div>
          <div style={{overflow:'scroll',height:'50vh',color:theme.colorT,marginBottom:65}}>
          <div className='Size20 boxRender' dangerouslySetInnerHTML={{__html: sezione?.testoIt?.html}} />
            <div className='p-0 mt-2'>
          

              {sound.map((s,i)=>{
                  return(
                    <>
                    <div onClick={()=>{selectAudio(i)}} style={{cursor:'pointer',alignItems: 'center'}} className='flex gap-2' key={'sound_'+i}>
                      <div style={{width:15,height:15,border:'1px solid #000',borderColor:theme.colorT,borderRadius:2,background:selecteedd(i) ? '#000' : 'transparent'}}></div>
                      <p style={{fontSize:12}}>{s.titolo}</p>
                      <img style={{height:20, maxWidth:'74%'}} src={theme.sound} />
                    </div>
                    <hr style={{borderColor: theme.colorT}} className='mb-2 mt-2'></hr>
                    </>
                  )
                })}
                {plll ? (
                  <div onClick={()=>{stop()}} style={{cursor:'pointer',alignItems: 'center', justifyContent: 'space-around',marginBottom:10}} className='flex gap-2' >
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M2 2h20v20h-20z"/></svg>
                  </div>
                ) : (
                  <div onClick={()=>{ selected.length > 0 ? playss() : console.log('no')}} style={{cursor:'pointer',alignItems: 'center', justifyContent: 'space-around',marginBottom:10}} className='flex gap-2' >
                  <svg fill={selected.length > 0 ? '#000' : '#c4c0c9'} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M3 22v-20l18 10-18 10z"/></svg>
                  </div>
                )}
                
                
            </div>
          </div>
        </main>
      </Layout>
    )
  } 



}
