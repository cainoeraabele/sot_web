import { useTheme , themes } from '../../lib/ThemeContext'
import Layout from '../../componets/layout'
import { GraphQLClient } from 'graphql-request'
import { useEffect, useState } from 'react'
import ReactAudioPlayer from 'react-audio-player';
import { isMobile } from 'react-device-detect';

const graphcms = new GraphQLClient('https://eu-central-1-shared-euc1-02.cdn.hygraph.com/content/clbcqkw4b11tf01tactj840qx/master')

export default function Home({sezione}) {
  const { theme, setTheme } = useTheme()
  const [sound,setSound] = useState([])
  const [ mobileV, setMobilev ] = useState(null)


  const toggleTheme = () => {
    setTheme(theme === themes.dark ? themes.light : themes.dark)
  }
  useEffect(()=>{
    setMobilev(isMobile)
  },isMobile)

  const soundList = async() =>{
    const query = `
      query {
        soundLogoElements {
          id
          titolo
          sound{
            url
          }
        }
      }
    `
    
    const sounds = await graphcms.request(query)

    console.log('vediamo');
    console.log(sounds.soundLogoElements);
    setSound(sounds.soundLogoElements)


    
    
  }

  useEffect(()=>{
    soundList()
  },[])

  
  if(mobileV){
    return (
      <Layout mobile>
      <main className='faddd'>
         <div>
            <h1 style={{fontSize:41,marginTop:0,color:theme.colorT}}>{sezione.titolo}</h1>
            <div className='flex gap-2'>
            {sezione.bottoni?.map((m,i)=>(
              <div key={'button'+i} style={{color:theme.colorT}}  className='bLink'>
                <button><a target={'_blank'} href={m.url} rel="noreferrer">{m.testo}</a></button>
              </div>
            ))}
          </div>
          </div>
        <div style={{background:theme.background,position:'relative', color:theme.colorT,marginBottom:65}}>
        <div className='Size20 boxRender' dangerouslySetInnerHTML={{__html: sezione.testoIt?.html}} />
            <div className='p-0 mt-10'>
                {sound.map((s,i)=>{
                  return(
                    <>
                    <div style={{alignItems: 'center', justifyContent: 'space-between'}} className='flex gap-2' key={'sound_'+i}>
                      <p style={{fontSize:12}}>{s.titolo}</p>
                      <ReactAudioPlayer
                        src={s.sound.url}
                        autoPlay
                        controls
                      />
                    </div>
                    <hr style={{borderColor: theme.colorT}} className='mb-5 mt-5'></hr>
                    </>
                  )
                })}
            </div>
        </div>
      </main>
      </Layout>
    )
  }else{
    return (
      <Layout>
      <main style={{height: '100%', display: 'grid',alignContent: 'space-between'}} className='faddd'>
          <div>
            <h1 style={{fontSize:41,marginTop:100,color:theme.colorT}}>{sezione.titolo}</h1>
            <div className='flex gap-2'>
            {sezione.bottoni?.map((m,i)=>(
              <div key={'button'+i} style={{color:theme.colorT}}  className='bLink'>
                <button><a target={'_blank'} href={m.url} rel="noreferrer">{m.testo}</a></button>
              </div>
            ))}
          </div>
          </div>
          <div style={{overflow:'scroll',height:'50vh',color:theme.colorT,marginBottom:65}}>
          <div className='Size20 boxRender' dangerouslySetInnerHTML={{__html: sezione.testoIt?.html}} />
            <div className='p-0 mt-10'>
                {sound.map((s,i)=>{
                  return(
                    <>
                    <div style={{alignItems: 'center', justifyContent: 'space-between'}} className='flex gap-2' key={'sound_'+i}>
                      <p style={{fontSize:12}}>{s.titolo}</p>
                      <ReactAudioPlayer
                        src={s.sound.url}
                        autoPlay
                        controls
                      />
                    </div>
                    <hr style={{borderColor: theme.colorT}} className='mb-5 mt-5'></hr>
                    </>
                  )
                })}
            </div>
          </div>
        </main>
      </Layout>
    )
  } 



}

// Query Data
export async function getStaticProps() {
  try {
    const query = `
      {
        sezione(where: {id: "clbczh55k1lcv0cw3wp7v1hpv"}) {
          testoIt {
            html
          }
          titolo
          bottoni {
            ... on Bottone {
              testo
              url
            }
          }
        }
      }
    `
      
    const { sezione }= await graphcms.request(query)

    console.log(sezione);
    return {
      props: {
        sezione: sezione ? sezione : {},
      },
    }
  } catch (error) {
    console.log(error)
  }

  return {
    props: {},
  }
}