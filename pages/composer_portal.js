import { useEffect, useState } from 'react'
import { useTheme , themes } from '../lib/ThemeContext'
import Layout from '../componets/layout'
import { GraphQLClient } from 'graphql-request'
import { isMobile } from 'react-device-detect';

const graphcms = new GraphQLClient('https://eu-central-1-shared-euc1-02.cdn.hygraph.com/content/clbcqkw4b11tf01tactj840qx/master')


export default function Home({}) {
  const { theme, setTheme } = useTheme()
  const [ mobileV, setMobilev ] = useState(null)
  const [ sezione, setSezione ] = useState(null)



     // Query Data
const call = async() =>{
  try {
    const query = `
      {
        sezione(where: {id: "clbcwwo6q1kaj0cw3p1vrul7c"}) {
          testoEn {
            html
          }
          titoloEn
          bottoni {
            ... on Bottone {
              testoEn
              url
            }
          }
        }
      }
    `
      
    const { sezione }= await graphcms.request(query)

    console.log(sezione);
    setSezione(sezione)
  } catch (error) {
    console.log(error)
  }
}

  useEffect(()=>{
    call()
  },[])

  
  const toggleTheme = () => {
    setTheme(theme === themes.dark ? themes.light : themes.dark)
  }

  useEffect(()=>{
    setMobilev(isMobile)
  },isMobile)



  if(mobileV){
    return (
      <Layout mobile>
      <main className='faddd'>
         <div>
            <h1 style={{fontSize:41,marginTop:0,color:theme.colorT}}>{sezione?.titoloEn}</h1>
            
          </div>
        <div style={{background:theme.background,position:'relative', color:theme.colorT,marginBottom:65}}>
        <div className='Size20 boxRender' dangerouslySetInnerHTML={{__html: sezione?.testoEn?.html}} />
        <div className='flex gap-2 mb-10'>
            {sezione?.bottoni?.map((m,i)=>(
              <div key={'button'+i} className='bLink'>
                <button><a target={'_blank'} href={m.url} rel="noreferrer">{m.testoEn}</a></button>
              </div>
            ))}
          </div>
            
        </div>
      </main>
      </Layout>
    )
  }else{
    return (
      <Layout>
        <main style={{height: '100%', display: 'grid',alignContent: 'space-between'}} className='faddd'>
          <div>
            <h1 style={{fontSize:41,marginTop:100,color:theme.colorT}}>{sezione?.titoloEn}</h1>    
          </div>
          <div style={{height:'50vh',marginBottom:65}}>
          <div style={{overflow:'scroll',height:'50vh',color:theme.colorT,paddingBottom: 70}}>
            <div className='Size20 boxRender' dangerouslySetInnerHTML={{__html: sezione?.testoEn?.html}} />
            <div className='flex gap-2'>
              {sezione?.bottoni?.map((m,i)=>(
                <div key={'button'+i} className='bLink'>
                  <button><a target={'_blank'} href={m.url} rel="noreferrer">{m.testoEn}</a></button>
                </div>
              ))}
            </div>
          </div>
          <div style={{backgroundColor:theme.colorBBB}} className='shadowns'></div>
        </div>
        </main>
      </Layout>
    )
  } 


}