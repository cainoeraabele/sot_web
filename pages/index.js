import { useEffect, useState } from 'react';
import { useTheme , themes } from '../lib/ThemeContext'
import Layout from '../componets/layout'
import { GraphQLClient } from 'graphql-request'
import { isMobile } from 'react-device-detect';

import Link from 'next/link'

const graphcms = new GraphQLClient('https://eu-central-1-shared-euc1-02.cdn.hygraph.com/content/clbcqkw4b11tf01tactj840qx/master')


export default function Home({}) {
  const { theme, setTheme } = useTheme()
  const [ mobileV, setMobilev ] = useState(null)
  const [ sezione, setSezione ] = useState(null)


  const toggleTheme = () => {
    setTheme(theme === themes.dark ? themes.light : themes.dark)
  }


    // Query Data
const call = async() =>{
  try {
    const query = `
      {
        sezione(where: {id: "clbcupgoh1iym0bulfkzwwwo2"}) {
          testoEn {
            html
          }
          titoloEn
          bottoni {
            ... on Bottone {
              testoEn
              url
            }
          }
        }
      }
    `
      
    const { sezione }= await graphcms.request(query)

    console.log(sezione);
    setSezione(sezione)
  } catch (error) {
    console.log(error)
  }
}

  useEffect(()=>{
    call()
  },[])


  useEffect(()=>{
    setMobilev(isMobile)
  },isMobile)


  if(mobileV){
    return (
      <Layout mobile>
      <main className='faddd'>
        <h1 style={{fontSize:41,marginBottom:50,marginTop:0,color:theme.colorT}}>{sezione?.titoloEn}</h1>
        <div style={{background:theme.background,position:'relative', color:theme.colorT,marginBottom:65}}>
          <div className='Size20 boxRender' dangerouslySetInnerHTML={{__html: sezione?.testoEn?.html}} />
        </div>
      </main>
      </Layout>
    )
  }else{
    return (
      <Layout>
      <main className='faddd'  >
        <h1 style={{fontSize:41,marginTop:100,color:theme.colorT}}>{sezione?.titoloEn}</h1>
        <div style={{height:'50vh',marginBottom:65}}>
          <div style={{overflow:'scroll',height:'50vh',color:theme.colorT,paddingBottom: 20}}>
            <div className='Size20 boxRender' dangerouslySetInnerHTML={{__html: sezione?.testoEn?.html}} />
          </div>
          <div style={{backgroundColor:theme.colorBBB}} className='shadowns'></div>
        </div>      
      </main>
    </Layout>
    )
  }  
}
