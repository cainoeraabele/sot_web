import { useTheme , themes } from '../lib/ThemeContext'
import Layout from '../componets/layout'
import { Formik, Field, Form ,errors } from 'formik';
import Link from 'next/link'
import * as Yup from "yup";

export default function Home() {
  const { theme, setTheme } = useTheme()
  

  const toggleTheme = () => {
    setTheme(theme === themes.dark ? themes.light : themes.dark)
  }



  return (
    <Layout>
    <main  className='faddd'  >
      <div>
        <h1 style={{fontSize:41,marginTop:100,color:theme.colorT}}>Contact</h1>
        <h2 style={{fontSize:20,color:theme.colorT}}>Please ask, we are listening:</h2>
        <div style={{overflow:'scroll',width:'100vw', maxHeight:'60vh',color:theme.colorT,marginBottom:65}}>
        <Formik
      initialValues={{
        firstName: '',
        lastName: '',
        email: '',
        text: '',
        privacy: false
      }}
      onSubmit={async (values) => {
        await new Promise((r) => setTimeout(r, 500));
        alert(JSON.stringify(values, null, 2));
      }}
      validationSchema={Yup.object().shape({
          email: Yup.string()
            .email()
            .required("Email Required"),
          firstName: Yup.string()
            .required("First Name Required"),
          lastName: Yup.string()
            .required("Last Name Required"),
          text: Yup.string()
          .required("Message Required"),
          privacy: Yup.bool().oneOf([true],'Accept Terms & Conditions is required'),
        })}
    >
      {props => {
        const {
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit
        } = props;
        return (
          <>
            <Form>
        <div className='formm' style={{marginTop:20, width:'40%',maxWidth:400, fontSize:18}}>
        {errors.firstName && touched.firstName && (
                <div className="input feedback">{errors.firstName}</div>
              )}
        <div style={{borderBottom:'1px solid', display:'flex'}}>
            <div style={{width:170}}>
              <p>NAME*</p>
            </div>
            <div>
              <Field id="firstName" name="firstName" placeholder="Jane" />
            </div>
          </div>
          {errors.lastName && touched.lastName && (
                <div className="input feedback">{errors.lastName}</div>
              )}
          <div style={{borderBottom:'1px solid',display:'flex'}}>
          <div style={{width:170}}>
              <p>LAST NAME*</p>
            </div>
            <div>
              <Field id="lastName" name="lastName" placeholder="Doe" />
            </div>
          </div>
           {errors.email && touched.email && (
                <div className="input feedback">{errors.email}</div>
              )}
          <div style={{borderBottom:'1px solid',display:'flex'}}>
            <div style={{width:170}}>  
              <p>EMAIL ADDRESS*</p>
            </div>
            <div>
            <Field
              id="email"
              name="email"
              placeholder="jane@acme.com"
              type="email"
            />
            </div>
          </div>
          {errors.text && touched.text && (
                <div className="input feedback">{errors.text}</div>
              )}
          <div style={{borderBottom:'1px solid',}}>
            <div>
              <p>MESSAGE*</p>
            </div>
            <div>
              <Field
                as='textarea'
                id="text"
                name="text"
                placeholder='...'
                style={{width:'100%',background:'transparent'}}
              />
            </div>
          </div>
          <br></br>
          {errors.privacy && touched.privacy && (
                <div className="input feedback">{errors.privacy}</div>
              )}
          <label style={{fontSize:14,paddingLeft:5}}>
            <Field style={{marginRight:10}} type="checkbox" name="privacy"  />
            I AGREE TO THE PRIVACY
          </label>
          <br></br>
          <br></br>
          <button style={{border: '1px solid #3E3E3E',filter: 'drop-shadow(0px 1px 1px rgba(0, 0, 0, 0.25))',padding:5, paddingLeft:20,paddingRight:20}} type="submit"><p style={{color:'#333'}}>SEND</p></button>
        </div>
      </Form>
          </>
        );
      }}
    </Formik>
      </div>
      </div>
    </main>
    </Layout>
  )
}