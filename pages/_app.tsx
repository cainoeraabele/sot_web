import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { ThemeProvider } from '../lib/ThemeContext'
import MatomoScript from '../componets/MatomoScript'


export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
    <MatomoScript siteId="1" matomoUrl="https://sotwebvercelapp"/>
    <ThemeProvider>
      <Component {...pageProps} />
    </ThemeProvider>
    </>
  )
}
