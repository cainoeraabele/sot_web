import React, { useContext, useState } from 'react'

export const themes = {
  light: {
    foreground: '#000000',
    background: '#E6E6E5',
    accents: '#eeee',
    logo: '/SOT-LOGO-FINAL.png',
    colorT:'#3E3E3E',
    border:'2px solid #333333',
    colorBBB: '#e6e6e5a3',
    fb:'/social/fbP.png',
    in:'/social/igP.png',
    tw:'/social/twitterP.png',
    vm:'/social/vimeoP.png',
    yt:'/social/ytP.png',
    tk:'/social/teleg.png',
    sound:'/img/wave.svg'
  },
  dark: {
    foreground: '#ffffff',
    background: '#18181a',
    accents: '#eeee',
    logo: '/SOT-LOGO-FINAL-BLACK-bacground 2.png',
    colorT:'#E2E2E2',
    border:'2px solid #E2E2E2',
    colorBBB: '#18181a80',
    fb:'/social/fbN.png',
    in:'/social/igN.png',
    tw:'/social/twitterN.png',
    vm:'/social/vimeoN.png',
    yt:'/social/ytN.png',
    tk:'/social/telegDk.png',
    sound:'/img/wave_neg.svg'
  },
}

export const ThemeContext = React.createContext({
  theme: undefined,
  setTheme: async (theme) => null,
})

export const useTheme = () => useContext(ThemeContext)

export const ThemeProvider = ({ children }) => {
  const [theme, setTheme] = useState(themes.light)

  return <ThemeContext.Provider value={{ theme, setTheme }}>{children}</ThemeContext.Provider>
}